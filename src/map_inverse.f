c-----------------------------------------------------------------------
c     subprogram 1. mapInverseEq(gIn,ts,io,eqIn,mEq)
c     reads and writes equilibrium data.
c     SCOTT:
c     With the mapmod update I have the direct data types filled and
c     have the higher-order-element gridding working, but I dislike the
c     method of filling the direct data structure.  It isn't as simple
c     as specifying R,Z and getting the values. Instead I still work in
c     psi and theta space, which would not be a problem if I could get
c     the quantities directly from the r2g0 and twod0 arrays, but for
c     some reason I couldn't do it on my first attempt.  This needs to 
c     be fixed eventually.
c-----------------------------------------------------------------------
      SUBROUTINE mapInverseEq(gIn,io,eqIn,mEq)  !#SIDL_SUB !#SIDL_SUB_NAME=map_inverse_eq#
      USE fgControlTypes
      USE fgInputEqTypes
      USE fgMapEqTypes
      USE fg_local
      USE fg_spline
      USE fg_bicube
      USE fg_physdat
      USE grid
      IMPLICIT none

      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlMap), INTENT(INOUT) :: gIn
      TYPE(inputEq), INTENT(INOUT) :: eqIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq

      INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
      INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
      INTEGER, PARAMETER :: niter = 20000
      TYPE(q_type) :: qdat
      INTEGER(i4) :: itau,itheta,ipsi,ia, iiter
      INTEGER(i4), DIMENSION(1) :: loc
      REAL(r8) :: r,z,br,bz,bt,jr,jz,jt,modpp,f,fp,p,pp,mach,machp
      REAL(r8) :: eta,theta,theta1,psifac,bp,rfac,eta0
      REAL(r8) :: v11,v12,v21,v22,g11,g22,g12,tau,jacfac,r2, jac
      REAL(r8) :: psi0, dpsi,vsnd,shift,nd,pe,vt,rnew,rold,dr,dz
      REAL(r8) :: psio
      REAL(r8), DIMENSION(:), ALLOCATABLE :: rcoord,taus,thetas,thpack
      REAL(r8), PARAMETER :: eps=1e-9
      TYPE(spline_type) :: dtheta,ff
      TYPE(bicube_type) :: r2g0,twod0
      INTEGER(i4) :: mt_in,ms_in,mvac,mpsi,mtheta,mxpie,pd_fg,pd_mesh
      CHARACTER(128) :: tt
c-----------------------------------------------------------------------
c     Convenient parameters
c-----------------------------------------------------------------------
      mvac=gIn%mvac; mpsi=gIn%mpsi; mtheta=gIn%mtheta; mxpie=gIn%mxpie
      pd_fg=gIn%pd; pd_mesh=gIn%pd_mesh
      psio=eqIn%psio; ms_in=eqIn%ms_in; mt_in=eqIn%mt_in
      ALLOCATE(rcoord(0:mpsi))
c-----------------------------------------------------------------------
c     Start off by copying over some of the original values
c-----------------------------------------------------------------------
      CALL copyMembers(gIn,eqIn,mEq)
c-----------------------------------------------------------------------
c     Sanity check
c-----------------------------------------------------------------------
      IF(gIn%grid_method == "original" .AND. ms_in /= mpsi) THEN
        WRITE(*,*) "Input grid size: ", ms_in+1
        WRITE(*,*) "Requested grid size: ", mpsi+1
        CALL fg_stop("Grid sizes must be same for grid_method=orginal")
      ENDIF
c-----------------------------------------------------------------------
c     Ensure consistency for inverse types
c-----------------------------------------------------------------------
      gIn%mvac=0                  ! Can't do vacuum
c-----------------------------------------------------------------------
c     prepare poloidal grids.
c-----------------------------------------------------------------------
      ALLOCATE(taus(0:mt_in),thetas(0:mt_in),thpack(0:mt_in))
      taus=twopi*(/(itau,itau=0,mt_in)/)/DFLOAT(mt_in)
      CALL spline_alloc(ff,mt_in,4_i4)
      CALL spline_alloc(dtheta,mt_in,3_i4)
      dtheta%xs=taus
c-----------------------------------------------------------------------
c     fill intermediate radial coordinates data structure
c-----------------------------------------------------------------------
      CALL bicube_alloc(r2g0,mt_in,ms_in,2_i4)
      DO ia=0,ms_in
       eta0=-twopi
       DO itau=0,mt_in-1
          dr=eqIn%rg_in(itau,ia)-mEq%ro; dz=eqIn%zg_in(itau,ia)-mEq%zo
          r2=dr**2+dz**2
          eta=ATAN2(dz,dr)
          IF(eta < eta0)eta = eta + twopi
          eta0=eta
          r2g0%fs(1,itau,ia)=r2
          r2g0%fs(2,itau,ia)=eta-taus(itau)
       ENDDO
       r2g0%fs(1:2,mt_in,ia)=r2g0%fs(1:2,0,ia)
      ENDDO
      r2g0%xs=taus
      r2g0%ys=eqIn%sq_in%xs
      CALL bicube_fit(r2g0,"periodic","extrap")
c-----------------------------------------------------------------------
c     Fit R,Z in new spline type to make interpolation easier
c-----------------------------------------------------------------------
      CALL bicube_alloc(twod0,mt_in,ms_in,2_i4)
      twod0%xs=taus
      twod0%ys=eqIn%sq_in%xs
      twod0%fs(1,:,:)=eqIn%rg_in(:,:)
      twod0%fs(2,:,:)=eqIn%zg_in(:,:)
      CALL bicube_fit(twod0,"periodic","extrap")
c-----------------------------------------------------------------------
c     Allocate fundamental data structures used in rest of fluxgrid
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%sq,mpsi,mEq%nsq)
      CALL bicube_alloc(r2g,mtheta,mpsi,2_i4)
      CALL bicube_alloc(mEq%twod,mtheta,mpsi,mEq%ntwod)
c-----------------------------------------------------------------------
c     Set up new radial grid.  The sq and twod arrays have psinormal as
c     the dependent variable.  The default is radial_variable=bigr which
c     means use a geometric based radial variable.  Need to use a Newton
c     iteration to find psi given a desired R.  This step is always the 
c     part that is likely to give problems.
c-----------------------------------------------------------------------
      IF (ms_in==mpsi) THEN
        WRITE(*,*) 'Setting the xs coordinate to be same as input code'
        mEq%sq%xs=eqIn%sq_in%xs
      ELSE
        CALL calc_psiofx(mEq,twod0%ys,twod0%fs(1,0,:))      ! calc rvss and svsr
        CALL psigrid(gIn,io,qdat,rcoord,
     &               eqIn%sq_in%xs,eqIn%sq_in%fs(:,3),pd_fg,pd_mesh)
        mpsi = SIZE(rcoord)-1

        IF (gIn%radial_variable=="psi") THEN
           mEq%sq%xs=rcoord
        ELSE
           DO ipsi=0,mpsi
             CALL spline_eval(svsr,rcoord(ipsi),1_i4)
             psi0=svsr%f(1)
             iiter=0
             DO
                CALL spline_eval(rvss,psi0,1_i4)
                dpsi=0.01*(rcoord(ipsi)-rvss%f(1))/rvss%f1(1)
                psi0=psi0+dpsi
                IF(ABS(dpsi) <= eps*psi0)EXIT
                iiter=iiter+1
                IF(iiter >= niter) THEN
                 WRITE(*,*) 
                 WRITE(*,*)"Error: Cannot create radial coordinate",ipsi
                 WRITE(*,*)"Suggestion: Use radial_method==power and"
                 WRITE(*,*)" increase radial_power.", iiter
                 STOP
                ENDIF 
                !WRITE(*,*) ipsi,psi0,dpsi,rcoord(ipsi),rvss%f(1)
             ENDDO
             mEq%sq%xs(ipsi)=psi0
           ENDDO
        ENDIF
      ENDIF
      CALL spline_dealloc(rvss)
      CALL spline_dealloc(svsr)
      ! We've found the radial grid
      r2g%ys=mEq%sq%xs; mEq%twod%ys=mEq%sq%xs
c-----------------------------------------------------------------------
c     Diagnose this new radial mesh
c-----------------------------------------------------------------------
      IF(io%bin_1d) THEN
        CALL open_bin(binary_unit,'pack.bin','UNKNOWN','REWIND',32_i4)
        ipsi=0
        rold=mEq%ro
        CALL bicube_eval(twod0,0._r8,mEq%sq%xs(ipsi),0_i4)
        rnew=twod0%f(1)
         WRITE(binary_unit)REAL(0.,4),
     $       REAL(mEq%sq%xs(ipsi),4),REAL(rnew,4),REAL(rnew-rold,4)
        DO ipsi=1,mpsi
          rold=rnew
          CALL bicube_eval(twod0,0._r8,mEq%sq%xs(ipsi),0_i4)
          rnew=twod0%f(1)
          WRITE(binary_unit)REAL(ipsi,4),
     $        REAL(mEq%sq%xs(ipsi),4),REAL(rnew,4),REAL(rnew-rold,4)
        ENDDO
        CALL close_bin(binary_unit,'pack.bin')
      ENDIF
c-----------------------------------------------------------------------
c     compute quantities on new mesh.
c     Doing something cheesy to get the jacobian - use rho, eta in center
c     and use R,Z on the outside.  Empirically works.
c     Need to  store r2g & twod info in ff since we don't know theta
c-----------------------------------------------------------------------
      DO ipsi=mpsi,0,-1
         psifac=mEq%sq%xs(ipsi)
         CALL spline_eval(eqIn%sq_in,psifac,1_i4)
         mEq%sq%fs(ipsi,:)=eqIn%sq_in%f(:)                  ! See global.f

         DO itau=0,mt_in
            tau=taus(itau)
            CALL bicube_eval(r2g0,tau,psifac,2_i4)
            CALL bicube_eval(twod0,tau,psifac,2_i4)
            thetas(itau)=tau+r2g0%f(2)          ! defines:angle_method='geom'
            rfac=SQRT(r2g0%f(1))
            r=twod0%f(1)
            IF (SQRT(mEq%sq%xs(ipsi)) < gIn%rjac) THEN
              jacfac=r2g0%fy(1)*(1+r2g0%fx(2))-r2g0%fx(1)*r2g0%fy(2)
            ELSE
              jacfac = 2.*(
     &         twod0%fy(1)*twod0%fx(2)-twod0%fx(1)*twod0%fy(2))
            ENDIF
            v11=(1+r2g0%fx(2))*2*rfac/jacfac
            v12=-r2g0%fx(1)/(rfac*jacfac)
            bp=SQRT(v11*v11+v12*v12)/r
            dtheta%fs(itau,1)=r*jacfac*bp**gIn%ipb/r**gIn%ipr
            dtheta%fs(itau,2)=jacfac/r
            dtheta%fs(itau,3)=r*jacfac*bp**gIn%jpb/r**gIn%jpr

            ff%fs(itau,1)=r2g0%f(1)       ! rho
            ff%fs(itau,2)=r2g0%f(2)       ! eta
            ff%fs(itau,3)=r               ! R
            ff%fs(itau,4)=twod0%f(2)      ! Z
         ENDDO        
c-----------------------------------------------------------------------
c        set up and spline to new poloidal grid
c-----------------------------------------------------------------------
         CALL spline_fit(dtheta,"periodic")
         CALL spline_int(dtheta)
         IF(TRIM(gIn%angle_method) == 'jac') THEN
           thetas=twopi*dtheta%fsi(:,1)/dtheta%fsi(mt_in,1)
         ENDIF
         IF (ipsi == mpsi) THEN
            thpack=twopi*dtheta%fsi(:,3)/dtheta%fsi(mt_in,3)
            CALL taugrid(gIn,thetas,thpack,r2g%xs,mt_in,pd_fg,pd_mesh)
         ENDIF

         ff%xs=thetas
         ff%fs(:,2)=ff%fs(:,2)+taus-thetas
         CALL spline_fit(ff,"periodic")
         DO itheta=0,mtheta
            CALL spline_eval(ff,r2g%xs(itheta),2_i4)
            r2g%fs(1:2,itheta,ipsi)=ff%f(1:2)
            mEq%twod%fs(1:2,itheta,ipsi)=ff%f(3:4)
         ENDDO
c                                                            Recompute q
         mEq%sq%fs(ipsi,3)=eqIn%sq_in%f(1)*dtheta%fsi(mt_in,2)
     $        /(4*pi*psio)
      ENDDO
      CALL bicube_dealloc(twod0)
c-----------------------------------------------------------------------
c     fit splines
c-----------------------------------------------------------------------
      CALL bicube_fit(r2g,"periodic","extrap")
      !CHAR tt='    psi   '//'    f     '//'    p     '//'    q     '
      !CHAR tt=TRIM(tt)//'   mach   '//'   ndens  '//'    pe    '
      !CHAR mEq%sq%title=tt
      CALL spline_fit(mEq%sq,"extrap")
c-----------------------------------------------------------------------
c     Now that we're on new grid, can finish twod
c-----------------------------------------------------------------------
      mEq%twod%xs=r2g%xs
      CALL bicube_alloc(twod0,mtheta,mpsi,2_i4)
      twod0%xs=mEq%twod%xs
      twod0%ys=mEq%twod%ys
      twod0%fs(1:2,:,:)=mEq%twod%fs(1:2,:,:)
      CALL bicube_fit(twod0,"periodic","extrap")
      IF(io%bin_2d) THEN
        CALL open_bin(binary_unit,'2d_jac.bin','UNKNOWN','REWIND',32_i4)
        DO ipsi=0,mpsi-1
         psifac=twod0%ys(ipsi)
         DO itheta=0,mtheta-1
            tau=twod0%xs(itheta)
            r=twod0%fs(1,itheta,ipsi)
            rfac=SQRT(r2g%fs(1,itheta,ipsi))
            IF (SQRT(mEq%sq%xs(ipsi)) < gIn%rjac) THEN
              jacfac=r2g%fsy(1,itheta,ipsi)*
     &                 (1.+r2g%fsx(2,itheta,ipsi))-
     &            r2g%fsx(1,itheta,ipsi)*r2g%fsy(2,itheta,ipsi)
            ELSE
              jacfac = 2.*(
     &         twod0%fsy(1,itheta,ipsi)*twod0%fsx(2,itheta,ipsi)-
     &          twod0%fsx(1,itheta,ipsi)*twod0%fsy(2,itheta,ipsi))
            ENDIF
            jac=r*jacfac/2./psio
            v11=(1+r2g%fsx(2,itheta,ipsi))*2*rfac/jacfac*psio
            v12=-r2g%fsx(1,itheta,ipsi)/(rfac*jacfac)*psio
            v21= r2g%fsy(2,itheta,ipsi) *2*rfac/jacfac
            v22=-r2g%fsy(1,itheta,ipsi)/(rfac*jacfac)

            g11= (v11*v11+v12*v12)                    ! g^(psi,psi)
            g12=-(v11*v21+v12*v22)                    ! g^(psi,theta)
            g22= (v21*v21+v22*v22)                    ! g^(theta,theta)

            mEq%twod%fs(3:6,itheta,ipsi)=(/jac,g11,g12,g22/)

            WRITE(binary_unit)(/REAL(psifac,4),REAL(tau,4),
     $      REAL(mEq%twod%fs(1:2,itheta,ipsi),4),
     $      REAL(jac,4),REAL(g11,4),REAL(g12,4),REAL(g22,4)/)
!            WRITE(*,*)(/REAL(psifac,4),REAL(tau,4),
!     $      REAL(mEq%twod%fs(1:2,itheta,ipsi),4),
!     $      REAL(jac,4),REAL(g11,4),REAL(g12,4),REAL(g22,4)/)
           ENDDO
          WRITE(binary_unit)
        ENDDO
        CALL close_bin(binary_unit,'2d_jac.bin')
      ENDIF
c-----------------------------------------------------------------------
c     fit splines
c-----------------------------------------------------------------------
      CALL bicube_fit(mEq%twod,"periodic","extrap")
c-----------------------------------------------------------------------
c     fit outer boundary to cubic splines.
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%ob,mtheta,4_i4)
      mEq%ob%xs=r2g%xs
      mEq%ob%fs(:,1:2)=TRANSPOSE(r2g%fs(:,:,mpsi))
      mEq%ob%fs(:,3:4)=TRANSPOSE(mEq%twod%fs(1:2,:,mpsi))
      CALL spline_fit(mEq%ob,"periodic")
c-----------------------------------------------------------------------
c     For inverse codes, the separatrix position will be taken as
c      the last surface.  We may want to be more sophisticated
c      later, but for now let rzsep = mEq%ob
c-----------------------------------------------------------------------
      CALL spline_alloc(mEq%rzsep,mtheta,2_i4)
      mEq%rzsep%xs=mEq%ob%xs
      mEq%rzsep%fs(:,1:2)=mEq%ob%fs(:,3:4)
      CALL spline_fit(mEq%rzsep,"periodic")

      loc=MINLOC(mEq%rzsep%fs(:,1))-1
      mEq%rsn = MINVAL(mEq%rzsep%fs(:,1))
      mEq%zsn = mEq%rzsep%fs(loc(1),2)

      loc=MAXLOC(mEq%rzsep%fs(:,1))-1
      mEq%rsx = MAXVAL(mEq%rzsep%fs(:,1))
      mEq%zsx = mEq%rzsep%fs(loc(1),2)

      loc=MINLOC(mEq%rzsep%fs(:,2))-1
      mEq%rsb = mEq%rzsep%fs(loc(1),1)
      mEq%zsb = MINVAL(mEq%rzsep%fs(:,2))

      loc=MAXLOC(mEq%rzsep%fs(:,2))-1
      mEq%rst = mEq%rzsep%fs(loc(1),1)
      mEq%zst = MAXVAL(mEq%rzsep%fs(:,2))
c-----------------------------------------------------------------------
c     find separatrix positions: rs1 and rs2
c-----------------------------------------------------------------------
      theta=pi
      DO 
         CALL spline_eval(mEq%ob,theta,1_i4) 
         theta1=(pi-mEq%ob%f(2)-theta)/(1+mEq%ob%f1(2))
         theta=theta+theta1
         IF(ABS(theta1) <= 1.e-10_r8) EXIT
      ENDDO
      CALL spline_eval(mEq%ob,theta,0_i4) 
      mEq%rs1=mEq%ro-SQRT(mEq%ob%f(1))
      mEq%rs2=mEq%ro+SQRT(mEq%ob%fs(0,1))
c      mEq%rs1=mEq%rsn
c      mEq%rs2=mEq%rsx
c-----------------------------------------------------------------------
c     If requested, fill the direct structure
c     Note: I think I could improve the accuracy by using two0 and
c     avoiding the resplining, but it doesn't seem to work. ?
c-----------------------------------------------------------------------
      IF (gIn%calc_direct) THEN
        CALL bicube_alloc(mEq%dir,mtheta,mpsi,mEq%ndir)
        mEq%dir%ys = mEq%twod%ys                  ! radial coord.
        mEq%dir%xs = mEq%twod%xs                  ! theta
        DO ipsi=0,mpsi
          psifac=mEq%sq%xs(ipsi)
          CALL spline_eval(eqIn%sq_in,psifac,1_i4)
          f  = eqIn%sq_in%f(1);      fp = eqIn%sq_in%f1(1)/psio
          p  = eqIn%sq_in%f(2);      pp = eqIn%sq_in%f1(2)/psio
          mach  = eqIn%sq_in%f(4);   machp = eqIn%sq_in%f1(4)/psio
          nd  = eqIn%sq_in%f(5)                 ! density
          pe  = eqIn%sq_in%f(6)                 ! electron pressure * mu0
          DO itheta=0,mtheta
            tau=twod0%xs(itheta)
            CALL bicube_eval(twod0,tau,psifac,2_i4)
            r=twod0%f(1);   z=twod0%f(2)
            jac = mEq%twod%fs(3,itheta,ipsi)
            br = twod0%fx(1)/jac
            bz = twod0%fx(2)/jac
            bt = f/r
            jr = -fp*br/mu0
            jz = -fp*bz/mu0
            shift=mach**2*((r/mEq%ro)**2-1)
            modpp=exp(shift)*(pp + p*2.*machp*mach*(r/mEq%ro)**2)
            jt = -(f*fp + r**2 * modpp)/(mu0*r**2)
            vsnd=(2.*p/mu0/nd/ms(2)/mEq%zz)**0.5
            vt=r/mEq%ro* mach*vsnd
            mEq%dir%fs(1,itheta,ipsi)=r
            mEq%dir%fs(2,itheta,ipsi)=z
            mEq%dir%fs(3,itheta,ipsi)=br
            mEq%dir%fs(4,itheta,ipsi)=bz
            mEq%dir%fs(5,itheta,ipsi)=bt
            mEq%dir%fs(6,itheta,ipsi)=jr
            mEq%dir%fs(7,itheta,ipsi)=jz
            mEq%dir%fs(8,itheta,ipsi)=jt
            mEq%dir%fs(9,itheta,ipsi)=vt
            mEq%dir%fs(10,itheta,ipsi)=p *EXP(shift)/mu0
            mEq%dir%fs(11,itheta,ipsi)=pe*EXP(shift)/mu0
            mEq%dir%fs(12,itheta,ipsi)=nd*EXP(shift)
            mEq%dir%fs(13,itheta,ipsi)=(1.-mEq%sq%xs(ipsi))*psio
            mEq%dir%fs(14,itheta,ipsi)=SQRT(mEq%sq%xs(ipsi))
          ENDDO
        ENDDO
        CALL bicube_fit(mEq%dir,"periodic","extrap")
      ENDIF
c-----------------------------------------------------------------------
c     Diagnose the mapping if asked for (2d.txt, 2d.bin)
c-----------------------------------------------------------------------
      IF(io%out_2d .OR. io%bin_2d) CALL write_2d(gIn,mEq,r2g,io)
c-----------------------------------------------------------------------
c     Now that we're done, we can deallocate stuff
c-----------------------------------------------------------------------
      CALL bicube_dealloc(r2g);     CALL bicube_dealloc(r2g0)
      CALL bicube_dealloc(twod0)
      DEALLOCATE(taus,thetas,thpack,rcoord)
!      CALL spline_dealloc(ff);      CALL spline_dealloc(dtheta)
c-----------------------------------------------------------------------
c     CONTAINED SUBROUTINES TO AVOID INTERFACE STATEMENTS OR MODULES
c-----------------------------------------------------------------------
      RETURN
      CONTAINS
c-----------------------------------------------------------------------
c       subprogram 2. calc_psiofx
c       Create a spline type of psi vs geometric radial coordinate x 
c       The geometrical x in this case is (R(ray)-Ro)/DeltaR) where the
c       ray chosen is from the O-point to the separatrix along midplane
c       to the separatrix.  See direct.f and grid.f
c       Note that psix(0) should not = 0 and bigr(0) should not equal ro
c       This will cause problems elsewhere in the code
c-----------------------------------------------------------------------
        SUBROUTINE calc_psiofx(mEq,psix,bigr)
        USE fg_local
        USE grid
        USE fgMapEqTypes
        IMPLICIT none
        
        TYPE(mappedEq), INTENT(INOUT) :: mEq
        REAL(r8), DIMENSION(0:), INTENT(IN) :: psix,bigr
        REAL(r8), PARAMETER :: eps=1e-10
        INTEGER(i4) :: ma
        INTEGER, PARAMETER :: ascii_unit = 41     ! ascii diagnostic file
        INTEGER, PARAMETER :: binary_unit = 42    ! binary diagnostic file
        LOGICAL, PARAMETER :: diagnose=.FALSE.
c-----------------------------------------------------------------------
        ma=SIZE(psix)-1
        CALL spline_alloc(rvss,ma,1_i4)
        rvss%xs=psix
        rvss%fs(:,1)=(bigr(:)-mEq%ro)/(bigr(ma)-mEq%ro)
        CALL spline_fit(rvss,"extrap")
  
        CALL spline_alloc(svsr,ma,1_i4)
        svsr%xs=rvss%fs(:,1)
        svsr%fs(:,1)=rvss%xs
        CALL spline_fit(svsr,"extrap")
c-----------------------------------------------------------------------
c       Diagnostics
c-----------------------------------------------------------------------
        IF (diagnose) THEN
          OPEN(UNIT=ascii_unit,FILE='rvss.txt',STATUS='UNKNOWN')
          CALL open_bin(binary_unit,'rvss.bin','UNKNOWN','REWIND',32_i4)
          !CHAR rvss%title='   psi         bigr   '
          CALL spline_write(rvss,.TRUE.,.TRUE.,ascii_unit,binary_unit)
          CALL close_bin(binary_unit,'rvss.bin')
          CLOSE(UNIT=ascii_unit)
  
          OPEN(UNIT=ascii_unit,FILE='svsr.txt',STATUS='UNKNOWN')
          CALL open_bin(binary_unit,'svsr.bin','UNKNOWN','REWIND',32_i4)
          !CHAR svsr%title='   bigr        psi  '
          CALL spline_write(svsr,.TRUE.,.TRUE.,ascii_unit,binary_unit)
          CALL close_bin(binary_unit,'svsr.bin')
          CLOSE(UNIT=ascii_unit)
        ENDIF
c-----------------------------------------------------------------------
c       terminate.
c-----------------------------------------------------------------------
        RETURN
        END SUBROUTINE calc_psiofx
c-----------------------------------------------------------------------
c     terminate routine.
c-----------------------------------------------------------------------
      END SUBROUTINE mapInverseEq
