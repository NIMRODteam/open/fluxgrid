c-----------------------------------------------------------------------
c     file analyze.f.
c     diagnosis equilibrium
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.
c-----------------------------------------------------------------------
c     1. calculate_fcirc.
c     2. fluxav.
c     3  find_rzextrema
c     4. fluxav.
c     5. q_find.
c     6. q_refine.
c     7. extrap.
c-----------------------------------------------------------------------
c     subprogram 1. calculate_fcirc.
c     Calculate useful stability parameters
c-----------------------------------------------------------------------
      MODULE fcirc_mod
      CONTAINS
      SUBROUTINE calculate_fcirc(mEq,fcirc)
      USE fg_local
      USE fg_physdat
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE

      TYPE(mappedEq), INTENT(IN) :: mEq
      REAL(r8), DIMENSION(0:), INTENT(INOUT) :: fcirc
      REAL(r8) :: r, fcl, fcu, bmax, f, gss
      REAL(r8), DIMENSION(:,:), ALLOCATABLE :: bmod, h, quantity
      REAL(r8), DIMENSION(:), ALLOCATABLE :: aveh1, aveh2, aveh3
      REAL(r8), DIMENSION(:), ALLOCATABLE :: aveh4, aveh5
      INTEGER(i4) :: mpsi,mtheta,ipsi,itheta
c-----------------------------------------------------------------------
c     Interfaces block
c-----------------------------------------------------------------------
      INTERFACE
        SUBROUTINE fluxav(mEq,quantity,average)
        USE fg_local
        USE fg_physdat
        USE fgAnalyzeTypes
        USE fgMapEqTypes
        TYPE(mappedEq), INTENT(IN) :: mEq
        REAL(r8), DIMENSION(:,:), INTENT(IN) :: quantity
        REAL(r8), DIMENSION(:), INTENT(OUT) :: average
        END SUBROUTINE fluxav
      END INTERFACE
c-----------------------------------------------------------------------
c     Allocate stability arrays
c-----------------------------------------------------------------------
      mpsi=mEq%mpsi; mtheta=mEq%mtheta
      ALLOCATE(bmod(0:mtheta,0:mpsi), h(0:mtheta,0:mpsi))
      ALLOCATE(aveh1(0:mpsi), aveh2(0:mpsi), aveh3(0:mpsi))
      ALLOCATE(aveh4(0:mpsi), aveh5(0:mpsi))  
      ALLOCATE(quantity(0:mtheta,0:mpsi))
c-----------------------------------------------------------------------
c     Load stuff up into arrays to make code more readable
c-----------------------------------------------------------------------
      DO ipsi = 0,mpsi
       f = mEq%sq%fs(ipsi,1)                         ! R B_tor
       DO itheta = 0,mtheta
        r = mEq%twod%fs(1,itheta,ipsi)
        gss = mEq%twod%fs(4,itheta,ipsi)
        bmod(itheta,ipsi) = (f**2 +gss)**0.5/r
       ENDDO
      ENDDO
c-----------------------------------------------------------------------
c     Calculate circulating particle fraction
c     References:
c       Lin-Liu & Miller Phys. Plas. 2 (1995) 1667
c		-- Provides remarkably good estimate of fcirc
c-----------------------------------------------------------------------
      DO ipsi = 0,mpsi
        bmax = MAXVAL(bmod(:,ipsi))
        DO itheta = 0,mtheta
              h(itheta,ipsi)= bmod(itheta,ipsi)/bmax
              h(itheta,ipsi)= MIN(h(itheta,ipsi),1.0_r8)
        ENDDO
      ENDDO

      CALL fluxav(mEq,h,aveh1)
      quantity(:,:) = h(:,:)**2
      CALL fluxav(mEq,quantity,aveh2)
      quantity(:,:) = h(:,:)**(-2)* (1._r8 - (1._r8 - h(:,:))**0.5
     &             * (0.5*h(:,:) + 1._r8)     )
      CALL fluxav(mEq,quantity,aveh3)

      DO ipsi = 0,mpsi
           fcl = aveh2(ipsi)*aveh3(ipsi)                    ! Lower estimate
           fcu = aveh2(ipsi)/aveh1(ipsi)**2 *
     &       ( 1._r8 -(1._r8 -aveh1(ipsi))**0.5
     &          *   (1._r8 + aveh1(ipsi)*0.5) )             ! Upper estimate
           fcirc(ipsi) = 0.75*fcu+0.25*fcl
      ENDDO
      DEALLOCATE(bmod, h, aveh1,aveh2,aveh3,aveh4,aveh5,quantity)
      RETURN
      END SUBROUTINE calculate_fcirc
      END MODULE fcirc_mod

c-----------------------------------------------------------------------
c     subprogram find_rzextrema
c     Use a spline interpolation to find the extrema of R,Z
c     More accurate than doing the simple MAXVAL/MINVAL used previously
c     rzextrema has the following structure:
c      rzextrema(1,:) : Rmin,   Z@Rmin
c      rzextrema(2,:) : Rmax,   Z@Rmax
c      rzextrema(3,:) : R@Zmin, Zmin
c      rzextrema(4,:) : R@Zmax, Zmax
c-----------------------------------------------------------------------
      SUBROUTINE find_rzextrema(rzvy,rzextrema)
      USE fg_local
      USE fg_physdat
      USE fg_spline
      IMPLICIT NONE
      REAL(r8), DIMENSION(:,0:), INTENT(IN) :: rzvy
      REAL(r8), DIMENSION(4,2), INTENT(OUT) :: rzextrema
      REAL(r8), PARAMETER :: eps=1.e-7
      INTEGER(i4) :: it, iloc, mt, iiter=0
      INTEGER(i4), DIMENSION(1) :: loc, its
      REAL(r8) :: tguess, dt, slowdown
      TYPE(spline_type) :: rzt
      REAL(r8) :: smallnum
      smallnum=SQRT(TINY(smallnum))
c-----------------------------------------------------------------------
c     Set up the spline type
c-----------------------------------------------------------------------
      slowdown=0.95
      mt=SIZE(rzvy,2)-1
      CALL spline_alloc(rzt,mt,2)
      rzt%xs(:)=(/(REAL(it,r8),it=0,mt)/)/REAL(mt,r8)*twopi
      rzt%fs(:,1)=rzvy(1,:); rzt%fs(:,2)=rzvy(2,:)
      CALL spline_fit(rzt,"periodic")
c-----------------------------------------------------------------------
c     Find rmin, rmax by Newton iteration R(t), Z(t)
c-----------------------------------------------------------------------
      loc=MINLOC(rzt%fs(:,1)); iloc=loc(1)-1
      tguess=REAL(iloc)/REAL(mt)*twopi
      its=0
      DO
         CALL spline_eval(rzt,tguess,2_i4)
         dt=slowdown*rzt%f1(1)/
     &       SIGN(MAX(ABS(rzt%f2(1)),smallnum),rzt%f2(1))
         its=its+1
         IF(ABS(dt) < eps) THEN
           rzextrema(1,1)=rzt%f(1)
           rzextrema(1,2)=rzt%f(2)
           EXIT
         ENDIF
         tguess=tguess-dt
      ENDDO

      loc=MAXLOC(rzt%fs(:,1)); iloc=loc(1)-1
      tguess=REAL(iloc)/REAL(mt)*twopi
      its=0
      DO
         CALL spline_eval(rzt,tguess,2_i4)
         dt=-slowdown*rzt%f1(1)/rzt%f2(1)
         !dt=slowdown*rzt%f1(1)/rzt%f(1)
         its=its+1
         IF(ABS(dt) < eps) THEN
           rzextrema(2,1)=rzt%f(1)
           rzextrema(2,2)=rzt%f(2)
           EXIT
         ENDIF
         tguess=tguess+dt
      ENDDO
c-----------------------------------------------------------------------
c     Find zmin, zmax by Newton iteration R(t), Z(t)
c     zmin often problematic because it can go through zero
c-----------------------------------------------------------------------
      loc=MINLOC(rzt%fs(:,2)); iloc=loc(1)-1
      tguess=REAL(iloc)/REAL(mt)*twopi
      iiter=0
      its=0
      DO
         iiter=iiter+1
         ! This prevents oscillations which occurs at small Z
         IF(MOD(iiter,50)==0) slowdown=slowdown*0.9
         CALL spline_eval(rzt,tguess,2_i4)
         dt=slowdown*rzt%f1(2)/
     &       SIGN(MAX(ABS(rzt%f2(2)),smallnum),rzt%f2(2))
         its=its+1
         IF(ABS(dt) < eps) THEN
           rzextrema(3,1)=rzt%f(1)
           rzextrema(3,2)=rzt%f(2)
           EXIT
         ENDIF
         tguess=tguess-dt
         !IF(tguess<0) tguess=twopi+tguess
      ENDDO

      loc=MAXLOC(rzt%fs(:,2)); iloc=loc(1)-1
      tguess=REAL(iloc)/REAL(mt)*twopi
      its=0
      DO
         CALL spline_eval(rzt,tguess,2_i4)
         dt=-slowdown*rzt%f1(2)/
     &       SIGN(MAX(ABS(rzt%f2(2)),smallnum),rzt%f2(2))
         its=its+1
         IF(ABS(dt) < eps) THEN
           rzextrema(4,1)=rzt%f(1)
           rzextrema(4,2)=rzt%f(2)
           EXIT
         ENDIF
         tguess=tguess+dt
      ENDDO
c-----------------------------------------------------------------------
c     Cleanup and return
c-----------------------------------------------------------------------
      CALL spline_dealloc(rzt)
      RETURN
      END SUBROUTINE find_rzextrema
c-----------------------------------------------------------------------
c     subprogram 3. fluxav.
c     Takes flux-surface average of 2D quantities
c-----------------------------------------------------------------------
      SUBROUTINE fluxav(mEq,quantity,average)
      USE fg_local
      USE fg_physdat
      USE fgAnalyzeTypes
      USE fgMapEqTypes

      TYPE(mappedEq), INTENT(IN) :: mEq
      REAL(r8), DIMENSION(0:,0:), INTENT(IN) :: quantity
      REAL(r8), DIMENSION(0:), INTENT(OUT) :: average
      TYPE(spline_type) :: intgd

      REAL(r8) :: jac
      INTEGER(i4) :: mpsi,mtheta,ipsi,itheta
      mpsi=mEq%mpsi; mtheta=mEq%mtheta
c-----------------------------------------------------------------------
c     integrate over each flux surface.
c-----------------------------------------------------------------------
      CALL spline_alloc(intgd,mtheta,2_i4)
      intgd%xs= mEq%twod%xs

      DO ipsi=0,mpsi
         DO itheta=0,mtheta
            jac = mEq%twod%fs(3,itheta,ipsi) 
            intgd%fs(itheta,1)= jac
            intgd%fs(itheta,2)=jac*quantity(itheta,ipsi)
         ENDDO
         CALL spline_fit(intgd,"periodic")
         CALL spline_int(intgd)
         average(ipsi)=intgd%fsi(mtheta,2)/intgd%fsi(mtheta,1)
      ENDDO

      CALL spline_dealloc(intgd)
c-----------------------------------------------------------------------
c     A routine just isn't a routine without the following comment:
c     terminate routine.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE fluxav
c-----------------------------------------------------------------------
c     subprogram 4. q_find.
c     Finds rational surfaces equal to qwant between psimin and psimax.
c-----------------------------------------------------------------------
      SUBROUTINE q_find(xsf,qsf,qrs,nrs,irs,numrs)
      USE fg_local
      USE fg_physdat
      IMPLICIT NONE
      INTEGER(i4), INTENT(IN) :: nrs
      REAL(r8), INTENT(IN) :: qrs
      REAL(r8), DIMENSION(0:), INTENT(IN) :: xsf,qsf
      INTEGER(i4), INTENT(OUT) :: numrs
      INTEGER(i4), DIMENSION(:), INTENT(INOUT) :: irs
      INTEGER(i4) :: mrs,ipsi
c-----------------------------------------------------------------------
c     This is just for finding out the number of rational surfaces
c     and location (between the relevant indices)
c-----------------------------------------------------------------------
      mrs=qrs*nrs
      numrs=0
      DO ipsi=0,SIZE(xsf)-2
        IF((qsf(ipsi)-qrs)*(qsf(ipsi+1)-qrs)<=0.) THEN
          numrs=numrs+1
          irs(numrs)=ipsi
        ENDIF
      ENDDO
      RETURN
      END SUBROUTINE q_find
c-----------------------------------------------------------------------
c     subprogram 4. q_refine.
c     Use Newton search to find more accurate location
c-----------------------------------------------------------------------
      SUBROUTINE q_refine(qprof,jrs,qrs,xrs,q1rs)
      USE fg_local
      USE fg_physdat
      USE fg_spline
      IMPLICIT NONE
      TYPE(spline_type), INTENT(INOUT) :: qprof
      INTEGER(i4), INTENT(IN) :: jrs
      REAL(r8), INTENT(IN) :: qrs
      REAL(r8), INTENT(OUT) :: xrs,q1rs
      INTEGER(i4) :: icnt
      INTEGER(i4), PARAMETER :: icnt_lim=10000
      REAL(r8), PARAMETER :: q_eps=1.e-9
      REAL(r8) :: xguess,qguess,q1guess
      REAL(r8) :: tinynum
      tinynum=SQRT(TINY(tinynum))
c-----------------------------------------------------------------------
c     This is just for finding out the number of rational surfaces
c     and location (between the relevant indices)
c-----------------------------------------------------------------------
      icnt=0
      xguess=qprof%xs(jrs)
      DO 
         icnt=icnt+1
         IF(icnt>icnt_lim) THEN
            WRITE(*,*) "Cannot find rational surface: ", jrs, qrs
            RETURN
         ENDIF
         CALL spline_eval(qprof,xguess,1_i4)
         qguess=qprof%f(1)
         q1guess=qprof%f1(1)
         IF(ABS(qguess-qrs)<q_eps) THEN
           xrs=xguess
           q1rs=q1guess
           RETURN
         ENDIF
         xguess=xguess-(qguess-qrs)/(q1guess+tinynum)
      ENDDO
      RETURN
      END SUBROUTINE q_refine
c-----------------------------------------------------------------------
c     subprogram 6. extrap.
c     extrapolates function to magnetic axis using polynomial approx.
c     The order of approximation = m
c     Number of points needed to extrapolate is m
c-----------------------------------------------------------------------
      SUBROUTINE extrap(m,x,ff,x0,ff0)
      USE fg_local
      USE fg_physdat
      IMPLICIT NONE

      INTEGER(i4), INTENT(IN) :: m
      REAL(r8), DIMENSION(m), INTENT(IN) :: x,ff
      REAL(r8), INTENT(IN) :: x0
      REAL(r8), INTENT(OUT) :: ff0
      
      INTEGER(i4) :: i,j
      REAL(r8) :: term
c-----------------------------------------------------------------------
c     computations.
c-----------------------------------------------------------------------
      ff0=0.
      DO i=1,m
         term=ff(i)
         DO j=1,m
            IF(j.NE.i)term=term*(x0-x(j))/(x(i)-x(j))
         ENDDO
         ff0=ff0+term
      ENDDO
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      RETURN
      END SUBROUTINE extrap
