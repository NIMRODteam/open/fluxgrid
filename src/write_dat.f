c-----------------------------------------------------------------------
c     file write_dat.f.
c     Writes ascii output file for Tecplot plotting program
c     Although it's using the Tecplot format, it should be easy to
c      modify for use in other programs
c
c     $Id: $
c
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c     code organization.  This is all one big subroutine
c-----------------------------------------------------------------------
c     1. Two dimensional data  (2d.dat)
c     2. stability data  (stability.dat)
c     3. NIMROD's field data (field.dat)
c     4. One dimensional profiles (1d.dat)
c     5. Transport coefficients (transport.dat)
c     6. Time scales (time_scales.dat)
c     7. 3D field lines (fline.dat)
c     8. outer boundary, separatrix
c-----------------------------------------------------------------------
      SUBROUTINE write_dat(io,gIn,mEq,gEV,gEP,sS,sT) !#SIDL_SUB !#SIDL_SUB_NAME=writeDat#
      USE fg_local
      USE fg_physdat
      USE fgControlTypes
      USE fgAnalyzeTypes
      USE fgMapEqTypes
      IMPLICIT NONE
      TYPE(fgControlIO), INTENT(IN) :: io
      TYPE(controlMap), INTENT(IN) :: gIn
      TYPE(mappedEq), INTENT(INOUT) :: mEq
      TYPE(singularSurfaces), INTENT(IN) :: sS
      TYPE(stabilityType), INTENT(IN) :: sT
      TYPE(globalEqDiagnose), INTENT(IN) :: gEV
      TYPE(globalEqProfiles), INTENT(IN) :: gEP

      INTEGER, PARAMETER :: tec2d = 18          ! tecplot output unit
      INTEGER, PARAMETER :: tec1d=19

      INTEGER(i4), PARAMETER :: mphi=90
      INTEGER(i4) :: itheta,ipsi,yspace, mnode,num_dp
      REAL(r8) :: tau,psi, bmod,gss,f,fprime,jac,ldelstr
      REAL(r8) :: r,z,x,y,phi,br,bz,bt,jr,jz,jt, p,conc
      REAL(r8) :: xold,yold,zold
      TYPE :: node_type
        REAL(r8) :: psirs,q
        INTEGER(i4) :: m,n, irs
        TYPE(node_type), POINTER :: next    ! ptr to next node in queue
      END TYPE node_type
      TYPE(node_type), POINTER :: rear
      REAL(r8) :: d2pdr2,psio
      REAL(r8), DIMENSION(:), ALLOCATABLE :: tplot,lemfp,tn
      REAL(r8), DIMENSION(:), ALLOCATABLE :: flchi,fhchi,eechi
      INTEGER(i4) :: mpsi,mtheta,mvac
      mpsi=mEq%mpsi; mtheta=mEq%mtheta; mvac=mEq%mvac; psio=mEq%psio
c-----------------------------------------------------------------------
c     Two dimensional data
c-----------------------------------------------------------------------
      ALLOCATE(tplot(0:mpsi),lemfp(0:mpsi),tn(0:mpsi))
      ALLOCATE(flchi(0:mpsi),fhchi(0:mpsi),eechi(0:mpsi))
      mnode=mpsi
      IF(mvac > 0) mnode=mpsi+mvac+1
      OPEN(UNIT=tec2d,FILE='2d.dat',STATUS='UNKNOWN')
      WRITE(tec2d,*) 'VARIABLES = "R", "Z", "psi"'
      WRITE(tec2d,*) 'ZONE ',',i=',mtheta+1,' j=',mnode+1,',F=POINT'
      DO itheta=0,mtheta
         WRITE(tec2d,*) mEq%ro,mEq%zo,0
!     &                  (mEq%sq%fs(0,1)-mEq%sq%fs1(0,1)*mEq%sq%xs(0))/ro
      ENDDO
      DO ipsi=0,mpsi
         psi=mEq%twod%ys(ipsi)
         !psi=psio*(1.-mEq%twod%ys(ipsi))
         DO itheta=0,mtheta
            tau=mEq%twod%xs(itheta)
            r=mEq%twod%fs(1,itheta,ipsi)
            z=mEq%twod%fs(2,itheta,ipsi)
            f=mEq%sq%fs(ipsi,1)
            gss=mEq%twod%fs(4,itheta,ipsi)
            bmod=(f**2 + gss)**0.5/r
            !WRITE(tec2d,*) r, z, SQRT(torflux(ipsi))
            !WRITE(tec2d,*) r, z, psi
            WRITE(tec2d,*) r, z, psi, tau, bmod
         ENDDO
      ENDDO
      IF(mvac > 0) THEN
      DO ipsi=1+mpsi,mvac+mpsi
         DO itheta=0,mtheta
           psi=mEq%dir%fs(13,itheta,ipsi)
           tau=mEq%twod%xs(itheta)
           r=mEq%dir%fs(1,itheta,ipsi)
           z=mEq%dir%fs(2,itheta,ipsi)
           br=mEq%dir%fs(3,itheta,ipsi)
           bz=mEq%dir%fs(4,itheta,ipsi)
           bt=mEq%dir%fs(5,itheta,ipsi)
           bmod=(br**2+bz**2+bt**2)**0.5
           WRITE(tec2d,*) r, z, psi, tau, bmod
         ENDDO
      ENDDO
      ENDIF
      CLOSE(UNIT=tec2d)
c-----------------------------------------------------------------------
c     Stability data
c-----------------------------------------------------------------------
      IF (io%stability) THEN
       OPEN(UNIT=tec1d,FILE='stability.dat',STATUS='UNKNOWN')
       WRITE(tec1d,*)
     &  "VARIABLES = `r,F',p',q',D_I,D_R, D_n_c,D_t_o_t,f_u_t,Hegna"
       WRITE(tec1d,*) "ZONE i=",mpsi+1, ", F=POINT"

       DO ipsi=0,mpsi
         WRITE(tec1d,*) SQRT(mEq%sq%xs(ipsi))
         WRITE(tec1d,*) mEq%sq%fs1(ipsi,1:3)/psio
         WRITE(tec1d,*) sT%dideal(ipsi)
         WRITE(tec1d,*) sT%dres(ipsi)
         WRITE(tec1d,*) sT%dnc(ipsi)
         WRITE(tec1d,*) SQRT(mEq%sq%xs(ipsi)),
     &    mEq%sq%fs1(ipsi,1:3)/psio,
     &    sT%dideal(ipsi), sT%dres(ipsi), sT%dnc(ipsi),
     &    sT%dres(ipsi)/(sT%alphas(ipsi)-sT%hfactor(ipsi))+sT%dnc(ipsi),
     &           sT%fcirc(ipsi),sT%alphas(ipsi)-sT%hfactor(ipsi)
       ENDDO

       CLOSE(UNIT=tec1d)
      ENDIF
c-----------------------------------------------------------------------
c     compute and write coordinates and equilibrium magnetic field.
c     This is what fluxgrid.dat contains so good for seeing what nimrod
c  is getting as input
c-----------------------------------------------------------------------
      OPEN(UNIT=tec2d,FILE="field.dat",STATUS="UNKNOWN")
      WRITE(tec2d,*) 'VARIABLES=R,Z,`y,`q,B_R,B_Z,B_t,J_R,J_Z,J_t,p,con'
      WRITE(tec2d,*) 'ZONE ',',i=',mtheta+1,' j=',mnode+1,',F=POINT'
      DO itheta=0,mtheta
         WRITE(tec2d,*) mEq%ro,mEq%zo,0, mEq%twod%xs(itheta), 0., 0.,
     &    (mEq%sq%fs(0,1)-mEq%sq%fs1(0,1)*mEq%sq%xs(0))/mEq%ro,
     &    0., 0., gEP%delstr(0,0),
     &    (mEq%sq%fs(0,2)-mEq%sq%fs1(0,2)*mEq%sq%xs(0))/mEq%ro,
     &                  0.
      ENDDO
      DO ipsi=0,mpsi
       psi=psio*(1.-mEq%twod%ys(ipsi))
       f = mEq%sq%fs(ipsi,1)                              ! R B_tor
       fprime = mEq%sq%fs1(ipsi,1)/psio                   ! dF/dpsi
       p = mEq%sq%fs(ipsi,2)                              ! R B_tor
c       pprime = mEq%sq%fs1(ipsi,2)/psio  		! mu0 pprime
       DO itheta=0,mtheta
           tau=mEq%twod%xs(itheta)
           ldelstr = gEP%delstr(itheta,ipsi)
           IF (gIn%j_t == "use_gs") ldelstr= gEP%gsrhs(itheta,ipsi)
           r = mEq%twod%fs(1,itheta,ipsi)
           z = mEq%twod%fs(2,itheta,ipsi)
           jac = mEq%twod%fs(3,itheta,ipsi)
           br = mEq%twod%fsx(1,itheta,ipsi)/jac
           bz = mEq%twod%fsx(2,itheta,ipsi)/jac
           bt = f/r

           jr = -fprime*br/mu0
           jz = -fprime*bz/mu0
           jt = ldelstr/(mu0*r**2)                    !Contravariant
c           conc=psi
           conc = SQRT(mEq%sq%xs(ipsi))
         WRITE(tec2d,*) r,z,psi,tau,br,bz,bt,jr,jz,jt,p,conc
       ENDDO
      ENDDO
      IF(mvac > 0) THEN
        DO ipsi=mpsi+1,mpsi+mvac
         DO itheta=0,mtheta
           psi=mEq%dir%fs(13,itheta,ipsi)
           tau=mEq%dir%xs(itheta)
           r=mEq%dir%fs(1,itheta,ipsi)
           z=mEq%dir%fs(2,itheta,ipsi)
           br=mEq%dir%fs(3,itheta,ipsi)
           bz=mEq%dir%fs(4,itheta,ipsi)
           bt=mEq%dir%fs(5,itheta,ipsi)
           jr=mEq%dir%fs(6,itheta,ipsi)
           jz=mEq%dir%fs(7,itheta,ipsi)
           jt=mEq%dir%fs(8,itheta,ipsi)
           p =mEq%dir%fs(10,itheta,ipsi)
           conc =mEq%dir%fs(14,itheta,ipsi)

         WRITE(tec2d,*) r,z,psi,tau,br,bz,bt,jr,jz,jt,p,conc
         ENDDO
        ENDDO
      ENDIF
      CLOSE(UNIT=tec2d)
c-----------------------------------------------------------------------
c     1D File - Surface quantities
c-----------------------------------------------------------------------
      OPEN(UNIT=tec1d,FILE='1d.dat',STATUS='UNKNOWN')
!CHAR        WRITE(tec1d,*)
!CHAR      &      "VARIABLES=",mEq%sq%title,",`r  <J.B/B^2>"
       WRITE(tec1d,*) "ZONE i=",mEq%sq%nodes+1, ", F=POINT"

       DO ipsi=0,mEq%sq%nodes
         WRITE(tec1d,*) mEq%sq%xs(ipsi), mEq%sq%fs(ipsi,1:mEq%sq%nqty),
     &           SQRT(mEq%sq%xs(ipsi)),  gEP%lambdaprof(ipsi)
       ENDDO

c-----------------------------------------------------------------------
c     1D File - Text Data (complicated because I want it to look nice)
c-----------------------------------------------------------------------

1700   FORMAT('TEXT X=35 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',a,'"')
1740   FORMAT('TEXT X=8 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',a4,'"')
1750   FORMAT('TEXT X=20 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="=','"')
1760   FORMAT('TEXT X=23 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',f10.2,'"')
1770   FORMAT('TEXT X=55 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',a4,'"')
1780   FORMAT('TEXT X=67 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="=','"')
1790   FORMAT('TEXT X=70 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',f10.2,'"')
1791   FORMAT('TEXT X=70 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',e9.2,'"')
1771   FORMAT('TEXT X=55 Y=',i3,1x,
     &       'F=HELV-BOLD HU=POINT H=14 T="',a6,'"')

       yspace=5  !Position text data in frame

       WRITE (tec1d,1700) yspace+85,'Global Data'

       WRITE (tec1d,1740) yspace+75,'B_o'
       WRITE (tec1d,1750) yspace+75
       WRITE (tec1d,1760) yspace+75,gEV%bt0

       WRITE (tec1d,1740) yspace+70,'I_P'
       WRITE (tec1d,1750) yspace+70
       WRITE (tec1d,1760) yspace+70,gEV%crnt

       WRITE (tec1d,1740) yspace+60,'`b  = '
       WRITE (tec1d,1750) yspace+60
       WRITE (tec1d,1760) yspace+60,100*gEV%betat

       WRITE (tec1d,1740) yspace+55,'`b_P'
       WRITE (tec1d,1750) yspace+55
       WRITE (tec1d,1760) yspace+55,gEV%betap1

       WRITE (tec1d,1740) yspace+50,'`b_N'
       WRITE (tec1d,1750) yspace+50
       WRITE (tec1d,1760) yspace+50,gEV%betan

       WRITE (tec1d,1740) yspace+45,'I_N'
       WRITE (tec1d,1750) yspace+45
       WRITE (tec1d,1760) yspace+45,gEV%crnt/(gEV%amean*gEV%bt0)

       WRITE (tec1d,1740) yspace+35,'q_o'
       WRITE (tec1d,1750) yspace+35
       WRITE (tec1d,1760) yspace+35,gEV%q0

       WRITE (tec1d,1740) yspace+30,'q_m'
       WRITE (tec1d,1750) yspace+30
       WRITE (tec1d,1760) yspace+30,gEV%qmin

       WRITE (tec1d,1740) yspace+25,'q_a'
       WRITE (tec1d,1750) yspace+25
       WRITE (tec1d,1760) yspace+25,gEV%qa

       WRITE (tec1d,1740) yspace+20,'l_i'
       WRITE (tec1d,1750) yspace+20
       WRITE (tec1d,1760) yspace+20,gEV%li1

       WRITE (tec1d,1770) yspace+75,'Z_o'
       WRITE (tec1d,1780) yspace+75
       WRITE (tec1d,1790) yspace+75, mEq%zo

       WRITE (tec1d,1770) yspace+70,'R_o'
       WRITE (tec1d,1780) yspace+70
       WRITE (tec1d,1790) yspace+70, mEq%ro

       WRITE (tec1d,1770) yspace+65,'<a>'
       WRITE (tec1d,1780) yspace+65
       WRITE (tec1d,1790) yspace+65,gEV%amean

       WRITE (tec1d,1770) yspace+60,'`k'
       WRITE (tec1d,1780) yspace+60
       WRITE (tec1d,1790) yspace+60,gEV%kappa

       WRITE (tec1d,1770) yspace+55,'`d'
       WRITE (tec1d,1780) yspace+55
       WRITE (tec1d,1790) yspace+55,gEV%delta1

       WRITE (tec1d,1770) yspace+50,'A'
       WRITE (tec1d,1780) yspace+50
       WRITE (tec1d,1790) yspace+50,gEV%aratio

       WRITE (tec1d,1770) yspace+35,'`t_A'
       WRITE (tec1d,1780) yspace+35
       WRITE (tec1d,1790) yspace+35,gEV%taua

       WRITE (tec1d,1770) yspace+30,'`t_R'
       WRITE (tec1d,1780) yspace+30
       WRITE (tec1d,1790) yspace+30,gEV%taur

       WRITE (tec1d,1770) yspace+25,'S'
       WRITE (tec1d,1780) yspace+25
       WRITE (tec1d,1790) yspace+25,gEV%taur/gEV%taua

       WRITE (tec1d,1770) yspace+20,'n'
       WRITE (tec1d,1780) yspace+20
       WRITE (tec1d,1791) yspace+20,gEV%nd0

       WRITE (tec1d,1771) yspace+15,'`h/`m_0'

      CLOSE(UNIT=tec1d)
c-----------------------------------------------------------------------
c     Write transport coefficients
c-----------------------------------------------------------------------
      OPEN(UNIT=tec1d,FILE='transport.dat',STATUS='UNKNOWN')
       WRITE(tec1d,*) "VARIABLES=i,elecd,chi_pll,chi_prp,S_b"
       WRITE(tec1d,*) "ZONE i=",SIZE(gEP%eta_prp), ", F=POINT"
       DO ipsi=0,mpsi
        WRITE(tec1d,*)ipsi, gEP%eta_prp(ipsi)/mu0, gEP%chi_pll(ipsi)
     &    ,gEP%chi_prp(ipsi),gEV%amean**2*mu0/gEP%eta_prp(ipsi)/gEV%taua
       ENDDO
      CLOSE(UNIT=tec1d)
c-----------------------------------------------------------------------
c     Write diffusive sources
c-----------------------------------------------------------------------
      OPEN(UNIT=tec2d,FILE='sources.dat',STATUS='UNKNOWN')
      WRITE(tec2d,*) "VARIABLES=R,Z,elecd,Jphi,elecdJphi,chiprp,chiprpd"
      WRITE(tec2d,*) 'ZONE ',',i=',mtheta+1,' j=',mnode,',F=POINT'
      DO ipsi=0,mpsi
         CALL spline_eval(mEq%sq,mEq%sq%xs(ipsi),2_i4)
         d2pdr2= mEq%sq%f2(2)
         DO itheta=0,mtheta
          r = mEq%twod%fs(1,itheta,ipsi)
          z = mEq%twod%fs(2,itheta,ipsi)
          WRITE(tec2d,*)r,z,
     &          gEP%eta_prp(ipsi)/mu0,jt,gEP%eta_prp(ipsi)/mu0*jt
     &          ,gEP%chi_prp(ipsi),gEP%chi_prp(ipsi)*d2pdr2
       ENDDO
      ENDDO
      ! This really needs to be redone.  Only true in separatrix
      IF(mvac > 0) THEN
        DO ipsi=mpsi+1,mpsi+mvac
         DO itheta=0,mtheta
           r=mEq%dir%fs(1,itheta,ipsi)
           z=mEq%dir%fs(2,itheta,ipsi)
           jt=mEq%dir%fs(8,itheta,ipsi)*r
          WRITE(tec2d,*)r,z,
     &             gEP%eta_prp(mpsi)/mu0,jt,gEP%eta_prp(mpsi)/mu0*jt,
     &             gEP%chi_prp(mpsi),0.
        ENDDO
       ENDDO
      ENDIF
      CLOSE(UNIT=tec2d)
c-----------------------------------------------------------------------
c     Write time scales as a function of radius
c-----------------------------------------------------------------------
      OPEN(UNIT=tec1d,FILE='time_scales.dat',STATUS='UNKNOWN')
       WRITE(tec1d,*) "VARIABLES=`r,`t_p_e,`t_c_e,`t_p_i,`t_c_i,`t_A,
     & `t_t_e,`t_t_i,`t_e,`t_i,`t_s_t_r,`t_e_n,"
       WRITE(tec1d,*) "ZONE i=",SIZE(gEP%eta_prp), ", F=BLOCK"
       WRITE(tec1d,*) SQRT(mEq%sq%xs(:))
       WRITE(tec1d,*) 1./(SQRT(mEq%sq%fs(:,5)*qs(1)**2/(eps0*ms(1))))
       WRITE(tec1d,*) 1./(-qs(1)*mEq%sq%fs(:,1)/gEV%rmean/ms(1))
       WRITE(tec1d,*) 1./(SQRT(mEq%sq%fs(:,5)*(mEq%zz*qs(1))**2/
     &                    eps0*ms(2)))
       WRITE(tec1d,*)1./((-mEq%zz*qs(1))*mEq%sq%fs(:,1)/gEV%rmean/ms(2))
       WRITE(tec1d,*) sqrt(gEV%tauafac*mEq%sq%fs(:,5))
       WRITE(tec1d,*) gEP%tau_e(:)
       WRITE(tec1d,*) gEP%tau_i(:)
       WRITE(tec1d,*) -1./
     &      (mEq%sq%fs(:,2)/(mEq%sq%fs(:,5)*qs(1)*gEV%amean**2*gEV%bt0))
       WRITE(tec1d,*) gEP%tau_i*(ms(2)/2./ms(1))
      CLOSE(UNIT=tec1d)
!TMPc-----------------------------------------------------------------------
!TMPc     Open file and write header
!TMPc-----------------------------------------------------------------------
!TMP      OPEN(UNIT=tec1d,FILE='fline.dat',STATUS='UNKNOWN')
!TMP      WRITE(tec1d,*) 'TITLE = "Field Line Plots"'
!TMP      WRITE(tec1d,*) 'VARIABLES = x, y, z, nxtheta, bmod, ds, s'
!TMP907   FORMAT('ZONE T="Outer Surface" F=POINT, I=',i5,' J=',i5)
!TMP908   FORMAT('ZONE T="m=',i2,',n=',i2,'" F=POINT,I=',i5)
!TMP909   FORMAT('ZONE T="Poloidal Mesh" F=POINT, I=',i5,' J=',i5)
!TMPcTMP909   FORMAT('ZONE T="Poloidal Mesh" F=POINT, I=',i5,' J=',i5,' K=',i5)
!TMPc-----------------------------------------------------------------------
!TMPc     Write outer flux surface because it looks better.
!TMPc-----------------------------------------------------------------------
!TMP      WRITE(tec1d,907) mtheta+1,mphi+1
!TMP      DO iphi=0,mphi
!TMP        phi=REAL(iphi)*pi/REAL(mphi)
!TMP        DO itheta=0,mtheta
!TMP           r=twod%fs(1,itheta,mpsi)
!TMP           z=twod%fs(2,itheta,mpsi)
!TMP           x=r*COS(phi);  y=r*SIN(phi)
!TMP           WRITE(tec1d,*) x,y,z,thetasf%fs(1,itheta,mpsi),ipsi,0.,0.
!TMP        ENDDO
!TMP      ENDDO
!TMPc-----------------------------------------------------------------------
!TMPc     Write Poloidal Mesh out
!TMPc-----------------------------------------------------------------------
!TMP      WRITE(tec1d,909) mpsi+1,mtheta+1
!TMP      iphi=0
!TMP      phi=REAL(iphi)*pi/REAL(mphi)
!TMP      DO itheta=0,mtheta
!TMP       DO ipsi=0,mpsi
!TMP         r=twod%fs(1,itheta,ipsi)
!TMP         z=twod%fs(2,itheta,ipsi)
!TMP         x=r*COS(phi);  y=r*SIN(phi)
!TMP         WRITE(tec1d,*) x,y,z,thetasf%fs(1,itheta,ipsi),ipsi,0.,0.
!TMP       ENDDO
!TMP      ENDDO
!TMPc-----------------------------------------------------------------------
!TMPc     On a field line, phi = q Theta_sf.  I also calculate s, ds, bmod
!TMPc     along the way just for curiosity.
!TMPc-----------------------------------------------------------------------
!TMP      rs => front
!TMP      DO WHILE(ASSOCIATED(rs))
!TMP        WRITE(tec1d,908) rs%m, rs%n, rs%n*mtheta+1
!TMP        s=0.; ds=0.
!TMP        itstart=0
!TMP        xold=0.;  yold=0.;  zold=0.
!TMP        DO iphi=1,rs%n
!TMP         IF(iphi > 1) itstart=1
!TMP         DO itheta=itstart,mtheta
!TMP          irs=rs%irs                                    ! Good enough
!TMP          r=twod%fs(1,itheta,irs)
!TMP          z=twod%fs(2,itheta,irs)
!TMP          phi=rs%m*thetasf%fs(1,itheta,irs)+(iphi-1)*2.*pi*rs%n
!TMP          x=r*COS(phi);  y=r*SIN(phi)
!TMP
!TMP          ds=((x-xold)**2+(y-yold)**2 +(z-zold)**2)**0.5
!TMP          s=s+ds
!TMP          f=mEq%sq%fs(irs,1);  gss=twod%fs(4,itheta,irs)
!TMP          bmod=(f**2 + gss)**0.5/r
!TMP
!TMP          WRITE(tec1d,*) x, y, z, phi/rs%q, bmod, ds , s
!TMP
!TMP          xold=x;  yold=y;  zold=z
!TMP         ENDDO
!TMP        ENDDO
!TMP        rs  => rs%next
!TMP      ENDDO
!TMP
!TMP      CLOSE(UNIT=tec1d)
c-----------------------------------------------------------------------
c     Outer boundary, separatrix, and wall info
c     Outer boundary being defined by psihigh, wall being defined as
c     edge of NIMROD's computational domain.
c-----------------------------------------------------------------------
      OPEN(UNIT=77,FILE="rzs.dat",STATUS="UNKNOWN")
      WRITE(77,*) 'VARIABLES = R_o,Z_o,R_s,Z_s,R_w,Z_w'
      WRITE(77,*) 'ZONE ',',i=',mtheta+1
      DO itheta=0,mtheta
        IF(mvac > 0) THEN
           WRITE(77,*) mEq%ob%fs(itheta,3:4),  mEq%rzsep%fs(itheta,1:2),
     &                 mEq%dir%fs(1:2,itheta,mvac+mpsi)
        ELSE
           WRITE(77,*) mEq%ob%fs(itheta,3:4),  mEq%rzsep%fs(itheta,1:2),
     &                 mEq%ob%fs(itheta,3:4)
        ENDIF
      ENDDO
      CLOSE(77)
c-----------------------------------------------------------------------
c     terminate.
c-----------------------------------------------------------------------
      DEALLOCATE(tplot,lemfp,tn)
      DEALLOCATE(flchi,fhchi,eechi)
      RETURN
      END SUBROUTINE write_dat
