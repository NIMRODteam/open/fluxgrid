######################################################################
#
# $Id: CMakeLists.txt 5373 2017-06-07 04:47:28Z tbechtel $
# CMakeLists.txt for nimlsode
#
######################################################################
#---------------------------------------------------------
# LSODE  (previously known as netlib_lite)
#---------------------------------------------------------

#TODO: should enable finding lsode separately

set(LSODE_SOURCES
  dbnorm.f90
  dcfode.f90
  dewset.f90
  dfnorm.f90
  dgbfa.f90
  dgbsl.f90
  dgefa.f90
  dgesl.f90
  dintdy.f90
  dmnorm.f90
  dprepj.f90
  dprja.f90
  drchek.f90
  droots.f90
  dsolsy.f90
  dsrcar.f90
  dsrcom.f90
  dstoda.f90
  dstode.f90
  dumach.f90
  dvnorm.f90
  iumach.f90
  ixsav.f90
  xerrwd.f90
  xsetf.f90
  xsetun.f90
  lsodar.f90
  lsode.f90
  lsode_module.f90
)

# These are blas routines
if (NOT DEFINED HAVE_BLAS)
  set(LSODE_SOURCES ${LSODE_SOURCES}
    daxpy.f90
    dcopy.f90
    ddot.f90
    dscal.f90
    idamax.f90
  )
endif ()

add_library(lsodelib ${LSODE_SOURCES})
if (HAVE_BLAS)
  target_link_libraries(lsodelib ${math_libs})
endif ()
