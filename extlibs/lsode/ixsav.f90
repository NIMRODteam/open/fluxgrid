      INTEGER FUNCTION IXSAV (IPAR, IVALUE, ISET) 
      implicit none
      integer :: iumach
!***BEGIN PROLOGUE  IXSAV                                               
!***SUBSIDIARY                                                          
!***PURPOSE  Save and recall error message control parameters.          
!***LIBRARY   MATHLIB                                                   
!***CATEGORY  R3C                                                       
!***TYPE      ALL (IXSAV-A)                                             
!***AUTHOR  Hindmarsh, Alan C., (LLNL)                                  
!***DESCRIPTION                                                         
!                                                                       
!  IXSAV saves and recalls one of two error message parameters:         
!    LUNIT, the logical unit number to which messages are printed, and  
!    MESFLG, the message print flag.                                    
!  This is a modification of the SLATEC library routine J4SAVE.         
!                                                                       
!  Saved local variables..                                              
!   LUNIT  = Logical unit number for messages.  The default is obtained 
!            by a call to IUMACH (may be machine-dependent).            
!   MESFLG = Print control flag..                                       
!            1 means print all messages (the default).                  
!            0 means no printing.                                       
!                                                                       
!  On input..                                                           
!    IPAR   = Parameter indicator (1 for LUNIT, 2 for MESFLG).          
!    IVALUE = The value to be set for the parameter, if ISET = .TRUE.   
!    ISET   = Logical flag to indicate whether to read or write.        
!             If ISET = .TRUE., the parameter will be given             
!             the value IVALUE.  If ISET = .FALSE., the parameter       
!             will be unchanged, and IVALUE is a dummy argument.        
!                                                                       
!  On return..                                                          
!    IXSAV = The (old) value of the parameter.                          
!                                                                       
!***SEE ALSO  XERMSG, XERRWD, XERRWV                                    
!***ROUTINES CALLED  IUMACH                                             
!***REVISION HISTORY  (YYMMDD)                                          
!   921118  DATE WRITTEN                                                
!   930329  Modified prologue to SLATEC format. (FNF)                   
!   930915  Added IUMACH call to get default output unit.  (ACH)        
!   930922  Minor cosmetic changes. (FNF)                               
!***END PROLOGUE  IXSAV                                                 
!                                                                       
! Subroutines called by IXSAV.. None                                    
! Function routine called by IXSAV.. IUMACH                             
!-----------------------------------------------------------------------
!**End                                                                  
      LOGICAL ISET 
      INTEGER IPAR, IVALUE 
!-----------------------------------------------------------------------
      INTEGER LUNIT, MESFLG 
!-----------------------------------------------------------------------
! The following Fortran-77 declaration is to cause the values of the    
! listed (local) variables to be saved between calls to this routine.   
!-----------------------------------------------------------------------
      SAVE LUNIT, MESFLG 
      DATA LUNIT/-1/, MESFLG/1/ 
!                                                                       
!***FIRST EXECUTABLE STATEMENT  IXSAV                                   
      IF (IPAR == 1) THEN 
        IF (LUNIT == -1) LUNIT = IUMACH() 
        IXSAV = LUNIT 
        IF (ISET) LUNIT = IVALUE 
        ENDIF 
!                                                                       
      IF (IPAR == 2) THEN 
        IXSAV = MESFLG 
        IF (ISET) MESFLG = IVALUE 
        ENDIF 
!                                                                       
      RETURN 
!----------------------- End of Function IXSAV -------------------------
      END                                           
