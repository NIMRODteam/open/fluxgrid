      subroutine dgbsl(abd,lda,n,ml,mu,ipvt,b,job) 
      implicit none
      integer lda,n,ml,mu,ipvt(1),job 
      real*8 abd(lda,1),b(1) 
!                                                                       
!     dgbsl solves the real*8 band system                               
!     a * x = b  or  trans(a) * x = b                                   
!     using the factors computed by dgbco or dgbfa.                     
!                                                                       
!     on entry                                                          
!                                                                       
!        abd     real*8(lda, n)                                         
!                the output from dgbco or dgbfa.                        
!                                                                       
!        lda     integer                                                
!                the leading dimension of the array  abd .              
!                                                                       
!        n       integer                                                
!                the order of the original matrix.                      
!                                                                       
!        ml      integer                                                
!                number of diagonals below the main diagonal.           
!                                                                       
!        mu      integer                                                
!                number of diagonals above the main diagonal.           
!                                                                       
!        ipvt    integer(n)                                             
!                the pivot vector from dgbco or dgbfa.                  
!                                                                       
!        b       real*8(n)                                              
!                the right hand side vector.                            
!                                                                       
!        job     integer                                                
!                = 0         to solve  a*x = b ,                        
!                = nonzero   to solve  trans(a)*x = b , where           
!                            trans(a)  is the transpose.                
!                                                                       
!     on return                                                         
!                                                                       
!        b       the solution vector  x .                               
!                                                                       
!     error condition                                                   
!                                                                       
!        a division by zero will occur if the input factor contains a   
!        zero on the diagonal.  technically this indicates singularity  
!        but it is often caused by improper arguments or improper       
!        setting of lda .  it will not occur if the subroutines are     
!        called correctly and if dgbco has set rcond .gt. 0.0           
!        or dgbfa has set info .eq. 0 .                                 
!                                                                       
!     to compute  inverse(a) * c  where  c  is a matrix                 
!     with  p  columns                                                  
!           call dgbco(abd,lda,n,ml,mu,ipvt,rcond,z)                    
!           if (rcond is too small) go to ...                           
!           do 10 j = 1, p                                              
!              call dgbsl(abd,lda,n,ml,mu,ipvt,c(1,j),0)                
!        10 continue                                                    
!                                                                       
!     linpack. this version dated 08/14/78 .                            
!     cleve moler, university of new mexico, argonne national lab.      
!                                                                       
!     subroutines and functions                                         
!                                                                       
!     blas daxpy,ddot                                                   
!     fortran min0                                                      
!                                                                       
!     internal variables                                                
!                                                                       
      real*8 ddot,t 
      integer k,kb,l,la,lb,lm,m,nm1 
!                                                                       
      m = mu + ml + 1 
      nm1 = n - 1 
      if (job /= 0) go to 50 
!                                                                       
!        job = 0 , solve  a * x = b                                     
!        first solve l*y = b                                            
!                                                                       
         if (ml == 0) go to 30 
         if (nm1 < 1) go to 30 
            do 20 k = 1, nm1 
               lm = min0(ml,n-k) 
               l = ipvt(k) 
               t = b(l) 
               if (l == k) go to 10 
                  b(l) = b(k) 
                  b(k) = t 
   10          continue 
               call daxpy(lm,t,abd(m+1,k),1,b(k+1),1) 
   20       continue 
   30    continue 
!                                                                       
!        now solve  u*x = y                                             
!                                                                       
         do 40 kb = 1, n 
            k = n + 1 - kb 
            b(k) = b(k)/abd(m,k) 
            lm = min0(k,m) - 1 
            la = m - lm 
            lb = k - lm 
            t = -b(k) 
            call daxpy(lm,t,abd(la,k),1,b(lb),1) 
   40    continue 
      go to 100 
   50 continue 
!                                                                       
!        job = nonzero, solve  trans(a) * x = b                         
!        first solve  trans(u)*y = b                                    
!                                                                       
         do 60 k = 1, n 
            lm = min0(k,m) - 1 
            la = m - lm 
            lb = k - lm 
            t = ddot(lm,abd(la,k),1,b(lb),1) 
            b(k) = (b(k) - t)/abd(m,k) 
   60    continue 
!                                                                       
!        now solve trans(l)*x = y                                       
!                                                                       
         if (ml == 0) go to 90 
         if (nm1 < 1) go to 90 
            do 80 kb = 1, nm1 
               k = n - kb 
               lm = min0(ml,n-k) 
               b(k) = b(k) + ddot(lm,abd(m+1,k),1,b(k+1),1) 
               l = ipvt(k) 
               if (l == k) go to 70 
                  t = b(l) 
                  b(l) = b(k) 
                  b(k) = t 
   70          continue 
   80       continue 
   90    continue 
  100 continue 
      return 
      END                                           
