      subroutine daxpy(n,da,dx,incx,dy,incy) 
      implicit none
!                                                                       
!     constant times a vector plus a vector.                            
!     uses unrolled loops for increments equal to one.                  
!     jack dongarra, linpack, 3/11/78.                                  
!                                                                       
      real*8 dx(1),dy(1),da 
      integer i,incx,incy,ix,iy,m,mp1,n 
!                                                                       
      if(n<=0)return 
      if (da == 0.0) return 
      if(incx==1.and.incy==1)go to 20 
!                                                                       
!        code for unequal increments or equal increments                
!          not equal to 1                                               
!                                                                       
      ix = 1 
      iy = 1 
      if(incx<0)ix = (-n+1)*incx + 1 
      if(incy<0)iy = (-n+1)*incy + 1 
      do 10 i = 1,n 
        dy(iy) = dy(iy) + da*dx(ix) 
        ix = ix + incx 
        iy = iy + incy 
   10 continue 
      return 
!                                                                       
!        code for both increments equal to 1                            
!                                                                       
!                                                                       
!        clean-up loop                                                  
!                                                                       
   20 m = mod(n,4) 
      if( m == 0 ) go to 40 
      do 30 i = 1,m 
        dy(i) = dy(i) + da*dx(i) 
   30 continue 
      if( n < 4 ) return 
   40 mp1 = m + 1 
      do 50 i = mp1,n,4 
        dy(i) = dy(i) + da*dx(i) 
        dy(i + 1) = dy(i + 1) + da*dx(i + 1) 
        dy(i + 2) = dy(i + 2) + da*dx(i + 2) 
        dy(i + 3) = dy(i + 3) + da*dx(i + 3) 
   50 continue 
      return 
      END                                           
