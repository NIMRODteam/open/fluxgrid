!-----------------------------------------------------------------------
!     file lsode_module.f                                               
!     This contains the explicit interface for lsode.                   
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!     code organization.                                                
!-----------------------------------------------------------------------
!     0.  lsode_mod.                                                    
!-----------------------------------------------------------------------
!     subprogram 0. lsode_mod                                           
!-----------------------------------------------------------------------
      MODULE lsode_mod 
      IMPLICIT NONE 
                                                                        
      INTERFACE exch_lagr 
      SUBROUTINE LSODE (F, NEQ, Y, T, TOUT, ITOL, RTOL, ATOL, ITASK,    &
     &                  ISTATE, IOPT, RWORK, LRW, IWORK, LIW, JAC, MF)  
        INTEGER, OPTIONAL :: MF 
        INTEGER, OPTIONAL :: JAC 
        EXTERNAL F 
        !EXTERNAL JAC                                                   
        INTEGER NEQ, ITOL, ITASK, ISTATE, IOPT, LRW, IWORK, LIW 
        REAL*8 Y, T, TOUT, RTOL, ATOL, RWORK 
        DIMENSION NEQ(*), Y(*), RTOL(*), ATOL(*), RWORK(LRW), IWORK(LIW) 
      END  SUBROUTINE 
      END INTERFACE 
!-----------------------------------------------------------------------
      END                                           
