      SUBROUTINE DSTODA (NEQ, Y, YH, NYH, YH1, EWT, SAVF, ACOR,         &
     &   WM, IWM, F, JAC, PJAC, SLVS)
      IMPLICIT NONE

      EXTERNAL F, JAC, PJAC, SLVS
      INTEGER NEQ, NYH, IWM
      REAL*8 Y, YH, YH1, EWT, SAVF, ACOR, WM
      DIMENSION NEQ(*), Y(*), YH(NYH,*), YH1(*), EWT(*), SAVF(*),       &
     &   ACOR(*), WM(*), IWM(*)
      INTEGER IOWND, IALTH, IPUP, LMAX, MEO, NQNYH, NSLP,               &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L,                     &
     &   LYH, LEWT, LACOR, LSAVF, LWM, LIWM, METH, MITER,               &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU
      INTEGER IOWND2, ICOUNT, IRFLAG, JTYP, MUSED, MXORDN, MXORDS
      REAL*8 CONIT, CRATE, EL, ELCO, HOLD, RMAX, TESCO,                 &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND
      REAL*8 ROWND2, CM1, CM2, PDEST, PDLAST, RATIO,                    &
     &   PDNORM
      COMMON /DLS001/ CONIT, CRATE, EL(13), ELCO(13,12),                &
     &   HOLD, RMAX, TESCO(3,12),                                       &
     &   CCMAX, EL0, H, HMIN, HMXI, HU, RC, TN, UROUND,                 &
     &   IOWND(6), IALTH, IPUP, LMAX, MEO, NQNYH, NSLP,                 &
     &   ICF, IERPJ, IERSL, JCUR, JSTART, KFLAG, L,                     &
     &   LYH, LEWT, LACOR, LSAVF, LWM, LIWM, METH, MITER,               &
     &   MAXORD, MAXCOR, MSBP, MXNCF, N, NQ, NST, NFE, NJE, NQU
      !$omp threadprivate(/DLS001/)
      COMMON /DLSA01/ ROWND2, CM1(12), CM2(5), PDEST, PDLAST, RATIO,    &
     &   PDNORM,                                                        &
     &   IOWND2(3), ICOUNT, IRFLAG, JTYP, MUSED, MXORDN, MXORDS
      !$omp threadprivate(/DLSA01/)
      INTEGER I, I1, IREDO, IRET, J, JB, M, NCF, NEWQ
      INTEGER LM1, LM1P1, LM2, LM2P1, NQM1, NQM2
      REAL*8 DCON, DDN, DEL, DELP, DSM, DUP, EXDN, EXSM, EXUP,          &
     &   R, RH, RHDN, RHSM, RHUP, TOLD, DMNORM
      REAL*8 ALPHA, DM1,DM2, EXM1,EXM2,                                 &
     &   PDH, PNORM, RATE, RH1, RH1IT, RH2, RM, SM1(12)
      DATA SM1/0.5, 0.575, 0.55, 0.45, 0.35, 0.25,                      &
     &   0.20, 0.15, 0.10, 0.075, 0.050, 0.025/
!-----------------------------------------------------------------------
! DSTODA performs one step of the integration of an initial value
! problem for a system of ordinary differential equations.
! Note: DSTODA is independent of the value of the iteration method
! indicator MITER, when this is .ne. 0, and hence is independent
! of the type of chord method used, or the Jacobian structure.
! Communication with DSTODA is done with the following variables:
!
! Y      = an array of length .ge. N used as the Y argument in
!          all calls to F and JAC.
! NEQ    = integer array containing problem size in NEQ(1), and
!          passed as the NEQ argument in all calls to F and JAC.
! YH     = an NYH by LMAX array containing the dependent variables
!          and their approximate scaled derivatives, where
!          LMAX = MAXORD + 1.  YH(i,j+1) contains the approximate
!          j-th derivative of y(i), scaled by H**j/factorial(j)
!          (j = 0,1,...,NQ).  On entry for the first step, the first
!          two columns of YH must be set from the initial values.
! NYH    = a constant integer .ge. N, the first dimension of YH.
! YH1    = a one-dimensional array occupying the same space as YH.
! EWT    = an array of length N containing multiplicative weights
!          for local error measurements.  Local errors in y(i) are
!          compared to 1.0/EWT(i) in various error tests.
! SAVF   = an array of working storage, of length N.
! ACOR   = a work array of length N, used for the accumulated
!          corrections.  On a successful return, ACOR(i) contains
!          the estimated one-step local error in y(i).
! WM,IWM = real and integer work arrays associated with matrix
!          operations in chord iteration (MITER .ne. 0).
! PJAC   = name of routine to evaluate and preprocess Jacobian matrix
!          and P = I - H*EL0*Jac, if a chord method is being used.
!          It also returns an estimate of norm(Jac) in PDNORM.
! SLVS   = name of routine to solve linear system in chord iteration.
! CCMAX  = maximum relative change in H*EL0 before PJAC is called.
! H      = the step size to be attempted on the next step.
!          H is altered by the error control algorithm during the
!          problem.  H can be either positive or negative, but its
!          sign must remain constant throughout the problem.
! HMIN   = the minimum absolute value of the step size H to be used.
! HMXI   = inverse of the maximum absolute value of H to be used.
!          HMXI = 0.0 is allowed and corresponds to an infinite HMAX.
!          HMIN and HMXI may be changed at any time, but will not
!          take effect until the next change of H is considered.
! TN     = the independent variable. TN is updated on each step taken.
! JSTART = an integer used for input only, with the following
!          values and meanings:
!               0  perform the first step.
!           .gt.0  take a new step continuing from the last.
!              -1  take the next step with a new value of H,
!                    N, METH, MITER, and/or matrix parameters.
!              -2  take the next step with a new value of H,
!                    but with other inputs unchanged.
!          On return, JSTART is set to 1 to facilitate continuation.
! KFLAG  = a completion code with the following meanings:
!               0  the step was succesful.
!              -1  the requested error could not be achieved.
!              -2  corrector convergence could not be achieved.
!              -3  fatal error in PJAC or SLVS.
!          A return with KFLAG = -1 or -2 means either
!          ABS(H) = HMIN or 10 consecutive failures occurred.
!          On a return with KFLAG negative, the values of TN and
!          the YH array are as of the beginning of the last
!          step, and H is the last step size attempted.
! MAXORD = the maximum order of integration method to be allowed.
! MAXCOR = the maximum number of corrector iterations allowed.
! MSBP   = maximum number of steps between PJAC calls (MITER .gt. 0).
! MXNCF  = maximum number of convergence failures allowed.
! METH   = current method.
!          METH = 1 means Adams method (nonstiff)
!          METH = 2 means BDF method (stiff)
!          METH may be reset by DSTODA.
! MITER  = corrector iteration method.
!          MITER = 0 means functional iteration.
!          MITER = JT .gt. 0 means a chord iteration corresponding
!          to Jacobian type JT.  (The DLSODA/DLSODAR argument JT is
!          communicated here as JTYP, but is not used in DSTODA
!          except to load MITER following a method switch.)
!          MITER may be reset by DSTODA.
! N      = the number of first-order differential equations.
!-----------------------------------------------------------------------
      KFLAG = 0
      TOLD = TN
      NCF = 0
      IERPJ = 0
      IERSL = 0
      JCUR = 0
      ICF = 0
      DELP = 0.0
      IF (JSTART > 0) GO TO 200
      IF (JSTART == -1) GO TO 100
      IF (JSTART == -2) GO TO 160
!-----------------------------------------------------------------------
! On the first call, the order is set to 1, and other variables are
! initialized.  RMAX is the maximum ratio by which H can be increased
! in a single step.  It is initially 1.E4 to compensate for the small
! initial H, but then is normally equal to 10.  If a failure
! occurs (in corrector convergence or error test), RMAX is set at 2
! for the next increase.
! DCFODE is called to get the needed coefficients for both methods.
!-----------------------------------------------------------------------
      LMAX = MAXORD + 1
      NQ = 1
      L = 2
      IALTH = 2
      RMAX = 10000.0
      RC = 0.0
      EL0 = 1.0
      CRATE = 0.7
      HOLD = H
      NSLP = 0
      IPUP = MITER
      IRET = 3
! Initialize switching parameters.  METH = 1 is assumed initially. -----
      ICOUNT = 20
      IRFLAG = 0
      PDEST = 0.0
      PDLAST = 0.0
      RATIO = 5.0
      CALL DCFODE (2, ELCO, TESCO)
      DO 10 I = 1,5
   10   CM2(I) = TESCO(2,I)*ELCO(I+1,I)
      CALL DCFODE (1, ELCO, TESCO)
      DO 20 I = 1,12
   20   CM1(I) = TESCO(2,I)*ELCO(I+1,I)
      GO TO 150
!-----------------------------------------------------------------------
! The following block handles preliminaries needed when JSTART = -1.
! IPUP is set to MITER to force a matrix update.
! If an order increase is about to be considered (IALTH = 1),
! IALTH is reset to 2 to postpone consideration one more step.
! If the caller has changed METH, DCFODE is called to reset
! the coefficients of the method.
! If H is to be changed, YH must be rescaled.
! If H or METH is being changed, IALTH is reset to L = NQ + 1
! to prevent further changes in H for that many steps.
!-----------------------------------------------------------------------
  100 IPUP = MITER
      LMAX = MAXORD + 1
      IF (IALTH == 1) IALTH = 2
      IF (METH == MUSED) GO TO 160
      CALL DCFODE (METH, ELCO, TESCO)
      IALTH = L
      IRET = 1
!-----------------------------------------------------------------------
! The el vector and related constants are reset
! whenever the order NQ is changed, or at the start of the problem.
!-----------------------------------------------------------------------
  150 DO 155 I = 1,L
  155   EL(I) = ELCO(I,NQ)
      NQNYH = NQ*NYH
      RC = RC*EL(1)/EL0
      EL0 = EL(1)
      CONIT = 0.5/(NQ+2)
      GO TO (160, 170, 200), IRET
!-----------------------------------------------------------------------
! If H is being changed, the H ratio RH is checked against
! RMAX, HMIN, and HMXI, and the YH array rescaled.  IALTH is set to
! L = NQ + 1 to prevent a change of H for that many steps, unless
! forced by a convergence or error test failure.
!-----------------------------------------------------------------------
  160 IF (H == HOLD) GO TO 200
      RH = H/HOLD
      H = HOLD
      IREDO = 3
      GO TO 175
  170 RH = MAX(RH,HMIN/ABS(H))
  175 RH = MIN(RH,RMAX)
      RH = RH/MAX(1.0,ABS(H)*HMXI*RH)
!-----------------------------------------------------------------------
! If METH = 1, also restrict the new step size by the stability region.
! If this reduces H, set IRFLAG to 1 so that if there are roundoff
! problems later, we can assume that is the cause of the trouble.
!-----------------------------------------------------------------------
      IF (METH == 2) GO TO 178
      IRFLAG = 0
      PDH = MAX(ABS(H)*PDLAST,0.000001)
      IF (RH*PDH*1.00001 < SM1(NQ)) GO TO 178
      RH = SM1(NQ)/PDH
      IRFLAG = 1
  178 CONTINUE
      R = 1.0
      DO 180 J = 2,L
        R = R*RH
        DO 180 I = 1,N
  180     YH(I,J) = YH(I,J)*R
      H = H*RH
      RC = RC*RH
      IALTH = L
      IF (IREDO == 0) GO TO 690
!-----------------------------------------------------------------------
! This section computes the predicted values by effectively
! multiplying the YH array by the Pascal triangle matrix.
! RC is the ratio of new to old values of the coefficient  H*EL(1).
! When RC differs from 1 by more than CCMAX, IPUP is set to MITER
! to force PJAC to be called, if a Jacobian is involved.
! In any case, PJAC is called at least every MSBP steps.
!-----------------------------------------------------------------------
  200 IF (ABS(RC-1.0) > CCMAX) IPUP = MITER
      IF (NST >= NSLP+MSBP) IPUP = MITER
      TN = TN + H
      I1 = NQNYH + 1
      DO 215 JB = 1,NQ
        I1 = I1 - NYH
!DIR$ IVDEP
        DO 210 I = I1,NQNYH
  210     YH1(I) = YH1(I) + YH1(I+NYH)
  215   CONTINUE
      PNORM = DMNORM (N, YH1, EWT)
!-----------------------------------------------------------------------
! Up to MAXCOR corrector iterations are taken.  A convergence test is
! made on the RMS-norm of each correction, weighted by the error
! weight vector EWT.  The sum of the corrections is accumulated in the
! vector ACOR(i).  The YH array is not altered in the corrector loop.
!-----------------------------------------------------------------------
  220 M = 0
      RATE = 0.0
      DEL = 0.0
      DO 230 I = 1,N
  230   Y(I) = YH(I,1)
      CALL F (NEQ, TN, Y, SAVF)
      NFE = NFE + 1
      IF (IPUP <= 0) GO TO 250
!-----------------------------------------------------------------------
! If indicated, the matrix P = I - H*EL(1)*J is reevaluated and
! preprocessed before starting the corrector iteration.  IPUP is set
! to 0 as an indicator that this has been done.
!-----------------------------------------------------------------------
      CALL PJAC (NEQ, Y, YH, NYH, EWT, ACOR, SAVF, WM, IWM, F, JAC)
      IPUP = 0
      RC = 1.0
      NSLP = NST
      CRATE = 0.7
      IF (IERPJ /= 0) GO TO 430
  250 DO 260 I = 1,N
  260   ACOR(I) = 0.0
  270 IF (MITER /= 0) GO TO 350
!-----------------------------------------------------------------------
! In the case of functional iteration, update Y directly from
! the result of the last function evaluation.
!-----------------------------------------------------------------------
      DO 290 I = 1,N
        SAVF(I) = H*SAVF(I) - YH(I,2)
  290   Y(I) = SAVF(I) - ACOR(I)
      DEL = DMNORM (N, Y, EWT)
      DO 300 I = 1,N
        Y(I) = YH(I,1) + EL(1)*SAVF(I)
  300   ACOR(I) = SAVF(I)
      GO TO 400
!-----------------------------------------------------------------------
! In the case of the chord method, compute the corrector error,
! and solve the linear system with that as right-hand side and
! P as coefficient matrix.
!-----------------------------------------------------------------------
  350 DO 360 I = 1,N
  360   Y(I) = H*SAVF(I) - (YH(I,2) + ACOR(I))
      CALL SLVS (WM, IWM, Y, SAVF)
      IF (IERSL < 0) GO TO 430
      IF (IERSL > 0) GO TO 410
      DEL = DMNORM (N, Y, EWT)
      DO 380 I = 1,N
        ACOR(I) = ACOR(I) + Y(I)
  380   Y(I) = YH(I,1) + EL(1)*ACOR(I)
!-----------------------------------------------------------------------
! Test for convergence.  If M .gt. 0, an estimate of the convergence
! rate constant is stored in CRATE, and this is used in the test.
!
! We first check for a change of iterates that is the size of
! roundoff error.  If this occurs, the iteration has converged, and a
! new rate estimate is not formed.
! In all other cases, force at least two iterations to estimate a
! local Lipschitz constant estimate for Adams methods.
! On convergence, form PDEST = local maximum Lipschitz constant
! estimate.  PDLAST is the most recent nonzero estimate.
!-----------------------------------------------------------------------
  400 CONTINUE
      IF (DEL <= 100.0*PNORM*UROUND) GO TO 450
      IF (M == 0 .AND. METH == 1) GO TO 405
      IF (M == 0) GO TO 402
      RM = 1024.0
      IF (DEL <= 1024.0*DELP) RM = DEL/DELP
      RATE = MAX(RATE,RM)
      CRATE = MAX(0.2*CRATE,RM)
  402 DCON = DEL*MIN(1.0,1.5*CRATE)/(TESCO(2,NQ)*CONIT)
      IF (DCON > 1.0) GO TO 405
      PDEST = MAX(PDEST,RATE/ABS(H*EL(1)))
      IF (PDEST /= 0.0) PDLAST = PDEST
      GO TO 450
  405 CONTINUE
      M = M + 1
      IF (M == MAXCOR) GO TO 410
      IF (M >= 2 .AND. DEL > 2.0*DELP) GO TO 410
      DELP = DEL
      CALL F (NEQ, TN, Y, SAVF)
      NFE = NFE + 1
      GO TO 270
!-----------------------------------------------------------------------
! The corrector iteration failed to converge.
! If MITER .ne. 0 and the Jacobian is out of date, PJAC is called for
! the next try.  Otherwise the YH array is retracted to its values
! before prediction, and H is reduced, if possible.  If H cannot be
! reduced or MXNCF failures have occurred, exit with KFLAG = -2.
!-----------------------------------------------------------------------
  410 IF (MITER == 0 .OR. JCUR == 1) GO TO 430
      ICF = 1
      IPUP = MITER
      GO TO 220
  430 ICF = 2
      NCF = NCF + 1
      RMAX = 2.0
      TN = TOLD
      I1 = NQNYH + 1
      DO 445 JB = 1,NQ
        I1 = I1 - NYH
!DIR$ IVDEP
        DO 440 I = I1,NQNYH
  440     YH1(I) = YH1(I) - YH1(I+NYH)
  445   CONTINUE
      IF (IERPJ < 0 .OR. IERSL < 0) GO TO 680
      IF (ABS(H) <= HMIN*1.00001) GO TO 670
      IF (NCF == MXNCF) GO TO 670
      RH = 0.25
      IPUP = MITER
      IREDO = 1
      GO TO 170
!-----------------------------------------------------------------------
! The corrector has converged.  JCUR is set to 0
! to signal that the Jacobian involved may need updating later.
! The local error test is made and control passes to statement 500
! if it fails.
!-----------------------------------------------------------------------
  450 JCUR = 0
      IF (M == 0) DSM = DEL/TESCO(2,NQ)
      IF (M > 0) DSM = DMNORM (N, ACOR, EWT)/TESCO(2,NQ)
      IF (DSM > 1.0) GO TO 500
!-----------------------------------------------------------------------
! After a successful step, update the YH array.
! Decrease ICOUNT by 1, and if it is -1, consider switching methods.
! If a method switch is made, reset various parameters,
! rescale the YH array, and exit.  If there is no switch,
! consider changing H if IALTH = 1.  Otherwise decrease IALTH by 1.
! If IALTH is then 1 and NQ .lt. MAXORD, then ACOR is saved for
! use in a possible order increase on the next step.
! If a change in H is considered, an increase or decrease in order
! by one is considered also.  A change in H is made only if it is by a
! factor of at least 1.1.  If not, IALTH is set to 3 to prevent
! testing for that many steps.
!-----------------------------------------------------------------------
      KFLAG = 0
      IREDO = 0
      NST = NST + 1
      HU = H
      NQU = NQ
      MUSED = METH
      DO 460 J = 1,L
        DO 460 I = 1,N
  460     YH(I,J) = YH(I,J) + EL(J)*ACOR(I)
      ICOUNT = ICOUNT - 1
      IF (ICOUNT >= 0) GO TO 488
      IF (METH == 2) GO TO 480
!-----------------------------------------------------------------------
! We are currently using an Adams method.  Consider switching to BDF.
! If the current order is greater than 5, assume the problem is
! not stiff, and skip this section.
! If the Lipschitz constant and error estimate are not polluted
! by roundoff, go to 470 and perform the usual test.
! Otherwise, switch to the BDF methods if the last step was
! restricted to insure stability (irflag = 1), and stay with Adams
! method if not.  When switching to BDF with polluted error estimates,
! in the absence of other information, double the step size.
!
! When the estimates are OK, we make the usual test by computing
! the step size we could have (ideally) used on this step,
! with the current (Adams) method, and also that for the BDF.
! If NQ .gt. MXORDS, we consider changing to order MXORDS on switching.
! Compare the two step sizes to decide whether to switch.
! The step size advantage must be at least RATIO = 5 to switch.
!-----------------------------------------------------------------------
      IF (NQ > 5) GO TO 488
      IF (DSM > 100.0*PNORM*UROUND .AND. PDEST /= 0.0)                  &
     &   GO TO 470
      IF (IRFLAG == 0) GO TO 488
      RH2 = 2.0
      NQM2 = MIN(NQ,MXORDS)
      GO TO 478
  470 CONTINUE
      EXSM = 1.0/L
      RH1 = 1.0/(1.2*DSM**EXSM + 0.0000012)
      RH1IT = 2.0*RH1
      PDH = PDLAST*ABS(H)
      IF (PDH*RH1 > 0.00001) RH1IT = SM1(NQ)/PDH
      RH1 = MIN(RH1,RH1IT)
      IF (NQ <= MXORDS) GO TO 474
         NQM2 = MXORDS
         LM2 = MXORDS + 1
         EXM2 = 1.0/LM2
         LM2P1 = LM2 + 1
         DM2 = DMNORM (N, YH(1,LM2P1), EWT)/CM2(MXORDS)
         RH2 = 1.0/(1.2*DM2**EXM2 + 0.0000012)
         GO TO 476
  474 DM2 = DSM*(CM1(NQ)/CM2(NQ))
      RH2 = 1.0/(1.2*DM2**EXSM + 0.0000012)
      NQM2 = NQ
  476 CONTINUE
      IF (RH2 < RATIO*RH1) GO TO 488
! THE SWITCH TEST PASSED.  RESET RELEVANT QUANTITIES FOR BDF. ----------
  478 RH = RH2
      ICOUNT = 20
      METH = 2
      MITER = JTYP
      PDLAST = 0.0
      NQ = NQM2
      L = NQ + 1
      GO TO 170
!-----------------------------------------------------------------------
! We are currently using a BDF method.  Consider switching to Adams.
! Compute the step size we could have (ideally) used on this step,
! with the current (BDF) method, and also that for the Adams.
! If NQ .gt. MXORDN, we consider changing to order MXORDN on switching.
! Compare the two step sizes to decide whether to switch.
! The step size advantage must be at least 5/RATIO = 1 to switch.
! If the step size for Adams would be so small as to cause
! roundoff pollution, we stay with BDF.
!-----------------------------------------------------------------------
  480 CONTINUE
      EXSM = 1.0/L
      IF (MXORDN >= NQ) GO TO 484
         NQM1 = MXORDN
         LM1 = MXORDN + 1
         EXM1 = 1.0/LM1
         LM1P1 = LM1 + 1
         DM1 = DMNORM (N, YH(1,LM1P1), EWT)/CM1(MXORDN)
         RH1 = 1.0/(1.2*DM1**EXM1 + 0.0000012)
         GO TO 486
  484 DM1 = DSM*(CM2(NQ)/CM1(NQ))
      RH1 = 1.0/(1.2*DM1**EXSM + 0.0000012)
      NQM1 = NQ
      EXM1 = EXSM
  486 RH1IT = 2.0*RH1
      PDH = PDNORM*ABS(H)
      IF (PDH*RH1 > 0.00001) RH1IT = SM1(NQM1)/PDH
      RH1 = MIN(RH1,RH1IT)
      RH2 = 1.0/(1.2*DSM**EXSM + 0.0000012)
      IF (RH1*RATIO < 5.0*RH2) GO TO 488
      ALPHA = MAX(0.001,RH1)
      DM1 = (ALPHA**EXM1)*DM1
      IF (DM1 <= 1000.0*UROUND*PNORM) GO TO 488
! The switch test passed.  Reset relevant quantities for Adams. --------
      RH = RH1
      ICOUNT = 20
      METH = 1
      MITER = 0
      PDLAST = 0.0
      NQ = NQM1
      L = NQ + 1
      GO TO 170
!
! No method switch is being made.  Do the usual step/order selection. --
  488 CONTINUE
      IALTH = IALTH - 1
      IF (IALTH == 0) GO TO 520
      IF (IALTH > 1) GO TO 700
      IF (L == LMAX) GO TO 700
      DO 490 I = 1,N
  490   YH(I,LMAX) = ACOR(I)
      GO TO 700
!-----------------------------------------------------------------------
! The error test failed.  KFLAG keeps track of multiple failures.
! Restore TN and the YH array to their previous values, and prepare
! to try the step again.  Compute the optimum step size for this or
! one lower order.  After 2 or more failures, H is forced to decrease
! by a factor of 0.2 or less.
!-----------------------------------------------------------------------
  500 KFLAG = KFLAG - 1
      TN = TOLD
      I1 = NQNYH + 1
      DO 515 JB = 1,NQ
        I1 = I1 - NYH
!DIR$ IVDEP
        DO 510 I = I1,NQNYH
  510     YH1(I) = YH1(I) - YH1(I+NYH)
  515   CONTINUE
      RMAX = 2.0
      IF (ABS(H) <= HMIN*1.00001) GO TO 660
      IF (KFLAG <= -3) GO TO 640
      IREDO = 2
      RHUP = 0.0
      GO TO 540
!-----------------------------------------------------------------------
! Regardless of the success or failure of the step, factors
! RHDN, RHSM, and RHUP are computed, by which H could be multiplied
! at order NQ - 1, order NQ, or order NQ + 1, respectively.
! In the case of failure, RHUP = 0.0 to avoid an order increase.
! The largest of these is determined and the new order chosen
! accordingly.  If the order is to be increased, we compute one
! additional scaled derivative.
!-----------------------------------------------------------------------
  520 RHUP = 0.0
      IF (L == LMAX) GO TO 540
      DO 530 I = 1,N
  530   SAVF(I) = ACOR(I) - YH(I,LMAX)
      DUP = DMNORM (N, SAVF, EWT)/TESCO(3,NQ)
      EXUP = 1.0/(L+1)
      RHUP = 1.0/(1.4*DUP**EXUP + 0.0000014)
  540 EXSM = 1.0/L
      RHSM = 1.0/(1.2*DSM**EXSM + 0.0000012)
      RHDN = 0.0
      IF (NQ == 1) GO TO 550
      DDN = DMNORM (N, YH(1,L), EWT)/TESCO(1,NQ)
      EXDN = 1.0/NQ
      RHDN = 1.0/(1.3*DDN**EXDN + 0.0000013)
! If METH = 1, limit RH according to the stability region also. --------
  550 IF (METH == 2) GO TO 560
      PDH = MAX(ABS(H)*PDLAST,0.000001)
      IF (L < LMAX) RHUP = MIN(RHUP,SM1(L)/PDH)
      RHSM = MIN(RHSM,SM1(NQ)/PDH)
      IF (NQ > 1) RHDN = MIN(RHDN,SM1(NQ-1)/PDH)
      PDEST = 0.0
  560 IF (RHSM >= RHUP) GO TO 570
      IF (RHUP > RHDN) GO TO 590
      GO TO 580
  570 IF (RHSM < RHDN) GO TO 580
      NEWQ = NQ
      RH = RHSM
      GO TO 620
  580 NEWQ = NQ - 1
      RH = RHDN
      IF (KFLAG < 0 .AND. RH > 1.0) RH = 1.0
      GO TO 620
  590 NEWQ = L
      RH = RHUP
      IF (RH < 1.1) GO TO 610
      R = EL(L)/L
      DO 600 I = 1,N
  600   YH(I,NEWQ+1) = ACOR(I)*R
      GO TO 630
  610 IALTH = 3
      GO TO 700
! If METH = 1 and H is restricted by stability, bypass 10 percent test.
  620 IF (METH == 2) GO TO 622
      IF (RH*PDH*1.00001 >= SM1(NEWQ)) GO TO 625
  622 IF (KFLAG == 0 .AND. RH < 1.1) GO TO 610
  625 IF (KFLAG <= -2) RH = MIN(RH,0.2)
!-----------------------------------------------------------------------
! If there is a change of order, reset NQ, L, and the coefficients.
! In any case H is reset according to RH and the YH array is rescaled.
! Then exit from 690 if the step was OK, or redo the step otherwise.
!-----------------------------------------------------------------------
      IF (NEWQ == NQ) GO TO 170
  630 NQ = NEWQ
      L = NQ + 1
      IRET = 2
      GO TO 150
!-----------------------------------------------------------------------
! Control reaches this section if 3 or more failures have occured.
! If 10 failures have occurred, exit with KFLAG = -1.
! It is assumed that the derivatives that have accumulated in the
! YH array have errors of the wrong order.  Hence the first
! derivative is recomputed, and the order is set to 1.  Then
! H is reduced by a factor of 10, and the step is retried,
! until it succeeds or H reaches HMIN.
!-----------------------------------------------------------------------
  640 IF (KFLAG == -10) GO TO 660
      RH = 0.1
      RH = MAX(HMIN/ABS(H),RH)
      H = H*RH
      DO 645 I = 1,N
  645   Y(I) = YH(I,1)
      CALL F (NEQ, TN, Y, SAVF)
      NFE = NFE + 1
      DO 650 I = 1,N
  650   YH(I,2) = H*SAVF(I)
      IPUP = MITER
      IALTH = 5
      IF (NQ == 1) GO TO 200
      NQ = 1
      L = 2
      IRET = 3
      GO TO 150
!-----------------------------------------------------------------------
! All returns are made through this section.  H is saved in HOLD
! to allow the caller to change H on the next step.
!-----------------------------------------------------------------------
  660 KFLAG = -1
      GO TO 720
  670 KFLAG = -2
      GO TO 720
  680 KFLAG = -3
      GO TO 720
  690 RMAX = 10.0
  700 R = 1.0/TESCO(2,NQU)
      DO 710 I = 1,N
  710   ACOR(I) = ACOR(I)*R
  720 HOLD = H
      JSTART = 1
      RETURN
!----------------------- End of Subroutine DSTODA ----------------------
      END