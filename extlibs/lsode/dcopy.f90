      subroutine dcopy(n,sx,incx,sy,incy) 
      implicit none
!                                                                       
!     copies a vector, x, to a vector, y.                               
!     uses unrolled loops for increments equal to 1.                    
!     jack dongarra, linpack, 3/11/78.                                  
!                                                                       
      real*8 sx(1),sy(1) 
      integer i,incx,incy,ix,iy,m,mp1,n 
!                                                                       
      if(n<=0)return 
      if(incx==1.and.incy==1)go to 20 
!                                                                       
!        code for unequal increments or equal increments                
!          not equal to 1                                               
!                                                                       
      ix = 1 
      iy = 1 
      if(incx<0)ix = (-n+1)*incx + 1 
      if(incy<0)iy = (-n+1)*incy + 1 
      do 10 i = 1,n 
        sy(iy) = sx(ix) 
        ix = ix + incx 
        iy = iy + incy 
   10 continue 
      return 
!                                                                       
!        code for both increments equal to 1                            
!                                                                       
!                                                                       
!        clean-up loop                                                  
!                                                                       
   20 m = mod(n,7) 
      if( m == 0 ) go to 40 
      do 30 i = 1,m 
        sy(i) = sx(i) 
   30 continue 
      if( n < 7 ) return 
   40 mp1 = m + 1 
      do 50 i = mp1,n,7 
        sy(i) = sx(i) 
        sy(i + 1) = sx(i + 1) 
        sy(i + 2) = sx(i + 2) 
        sy(i + 3) = sx(i + 3) 
        sy(i + 4) = sx(i + 4) 
        sy(i + 5) = sx(i + 5) 
        sy(i + 6) = sx(i + 6) 
   50 continue 
      return 
      END                                           
