      integer function idamax(n,dx,incx) 
      implicit none
!                                                                       
!     finds the index of element having max. absolute value.            
!     jack dongarra, linpack, 3/11/78.                                  
!                                                                       
      real*8 dx(1),dmax 
      integer i,incx,ix,n 
!                                                                       
      idamax = 0 
      if( n < 1 ) return 
      idamax = 1 
      if(n==1)return 
      if(incx==1)go to 20 
!                                                                       
!        code for increment not equal to 1                              
!                                                                       
      ix = 1 
      dmax = abs(dx(1)) 
      ix = ix + incx 
      do 10 i = 2,n 
         if(abs(dx(ix))<=dmax) go to 5 
         idamax = i 
         dmax = abs(dx(ix)) 
    5    ix = ix + incx 
   10 continue 
      return 
!                                                                       
!        code for increment equal to 1                                  
!                                                                       
   20 dmax = abs(dx(1)) 
      do 30 i = 2,n 
         if(abs(dx(i))<=dmax) go to 30 
         idamax = i 
         dmax = abs(dx(i)) 
   30 continue 
      return 
      END                                           
