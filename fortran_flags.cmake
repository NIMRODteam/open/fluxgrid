#---------------------------------------------------------------------
# Set compiler specific definitions and flags. 
# Valid build types are Debug, RelWithDebInfo 
# and Release. For the PGI compiler a custom build type FastBuild is 
# defined to aid develpment without the fastsse flag. Set endian and 
# underscore flags for all builds.
#---------------------------------------------------------------------
if (${CMAKE_Fortran_COMPILER_ID} MATCHES "Cray")
  option(__cray "using Cray FC Compiler" on)
  set(CMAKE_Fortran_FLAGS "-h nosecond_underscore -s real64 ${CMAKE_Fortran_FLAGS}")
  if (NOT ${ENABLE_NATIVE_ENDIAN})
    set(CMAKE_Fortran_FLAGS "-h byteswapio ${CMAKE_Fortran_FLAGS}")
  endif ()
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "GNU")
  option(__gfortran "using gfortran FC Compiler" on)
  set(CMAKE_Fortran_FLAGS "-fno-second-underscore -fdefault-real-8 -fdefault-double-8 ${CMAKE_Fortran_FLAGS}")
  #set(CMAKE_Fortran_FLAGS "-Wimplicit-interface -Wimplicit-procedure ${CMAKE_Fortran_FLAGS}")
  # Don't allow automatic reallocation (F2003 standard) as it may be inefficient
  # inside loops, flag only needed for gfortran versions > 4.6  
  set(CMAKE_Fortran_FLAGS "-fno-realloc-lhs ${CMAKE_Fortran_FLAGS}")
  # Turn argument mismatch into a warning until fixed by updating MPI interface
  if (CMAKE_Fortran_COMPILER_VERSION VERSION_GREATER_EQUAL 10.0)
    set(CMAKE_Fortran_FLAGS "-fallow-argument-mismatch ${CMAKE_Fortran_FLAGS}")
  endif ()
  if (NOT ${ENABLE_NATIVE_ENDIAN})
    set(CMAKE_Fortran_FLAGS "-fconvert=big-endian ${CMAKE_Fortran_FLAGS}")
  endif ()
  # Add trap flags and bounds checking if not debug and requested 
  # also enable backtraces upon error
  if (TRAP_FP_EXCEPTIONS)
    if (${CMAKE_BUILD_TYPE_UC} MATCHES "DEBUG") 
      set(CMAKE_Fortran_FLAGS "-ffpe-trap=invalid,zero,overflow ${CMAKE_Fortran_FLAGS}")
    else ()
      set(CMAKE_Fortran_FLAGS "-g -fbounds-check -fbacktrace -ffpe-trap=invalid,zero,overflow ${CMAKE_Fortran_FLAGS}")
    endif ()
  endif ()
  set(CMAKE_Fortran_FLAGS_RELEASE "-O3")
  set(CMAKE_C_FLAGS_RELEASE "-O3")
  set(CMAKE_Fortran_FLAGS_DEBUG "-O0 -g -fbounds-check -fbacktrace")
  set(CMAKE_C_FLAGS_DEBUG "-O0 -g -fbounds-check")
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "Intel")
  option(__ifort "using Intel FC Compiler" on)
  set(CMAKE_Fortran_FLAGS "-assume no2underscores,protect_parens -autodouble ${CMAKE_Fortran_FLAGS}")
  if (NOT ${ENABLE_NATIVE_ENDIAN})
    set(CMAKE_Fortran_FLAGS "-convert big_endian ${CMAKE_Fortran_FLAGS}")
  endif ()
  set(CMAKE_Fortran_FLAGS_DEBUG "-O0 -g -check all -traceback")
  set(CMAKE_C_FLAGS_DEBUG "-O0 -g -traceback")
elseif (${CMAKE_Fortran_COMPILER_ID} MATCHES "PGI")
  option(__pgi "using PGI FC Compiler" on)
  set(CMAKE_Fortran_FLAGS "-Mextend -r8 ${CMAKE_Fortran_FLAGS}")
  if (NOT ${ENABLE_NATIVE_ENDIAN})
    set(CMAKE_Fortran_FLAGS "-byteswapio ${CMAKE_Fortran_FLAGS}")
  endif ()
  set(CMAKE_Fortran_FLAGS_RELEASE "-O3 -fastsse")
  set(CMAKE_C_FLAGS_RELEASE "-O3 -fastsse")
  set(CMAKE_Fortran_FLAGS_FASTBUILD "-O3")
  set(CMAKE_C_FLAGS_FASTBUILD "-O3")
  set(CMAKE_Fortran_FLAGS_DEBUG "-g -O0 -Mbounds")
  set(CMAKE_C_FLAGS_DEBUG "-g -O0 -Mbounds")
endif ()

# Remove fpic flags
string(REPLACE "-fpic" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")
string(REPLACE "-fPIC" "" CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS}")

# Write flags
message(STATUS "")
message(STATUS "--------- Final compiler flags ---------")
foreach (cmp C Fortran)
  foreach (bld FULL RELEASE RELWITHDEBINFO MINSIZEREL DEBUG)
    message(STATUS "CMAKE_${cmp}_FLAGS_${bld}= ${CMAKE_${cmp}_FLAGS_${bld}}")
  endforeach ()
  message(STATUS "CMAKE_${cmp}_FLAGS= ${CMAKE_${cmp}_FLAGS}")
endforeach ()
message(STATUS "")
set(CMAKE_Fortran_BUILD_TYPE_FLAGS "${CMAKE_Fortran_FLAGS_${CMAKE_BUILD_TYPE_UC}}")
message(STATUS "CMAKE_Fortran_BUILD_TYPE_FLAGS= ${CMAKE_Fortran_BUILD_TYPE_FLAGS}")

#---------------------------------------------------------------------
# Always use rpath to greatest extent.
#---------------------------------------------------------------------

if (NOT DEFINED CMAKE_INSTALL_RPATH_USE_LINK_PATH)
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif ()
