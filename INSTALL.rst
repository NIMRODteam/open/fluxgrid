
Installation instructions
=========================

Fluxgrid uses CMake for it's build system.  

The simplest build system looks as follows::

      mkdir build
      cd build
      cmake ..
      make

The options are specified with the `-D` format.  An example configure script
using this would be::

      #!/bin/sh
      cmake \
        -DHDF5_DIR:PATH=$HDF_ROOT \
        VERBOSE=1 \
        ..

More details on using cmake can be found at https://cmake.org


The most important external dependency for fluxgrid is `lsode`, an ODE
integration package that is widely used in the fusion community (it is the
pregenetor of CVODE, and what is now known as SUNDIALS).  At present, fluxgrid
builds it's own version but 

HDF5 is the most common binary format used by fluxgrid.  Prior versions used
netcdf, which is used by PLASMA_STATE.

